function fixMapsPolos($) {
  var polos = $('.panel-polo');

  if (polos.length > 0) {
    $.each(polos, function(index, val) {
       var height = $(val).find('.polo-informacoes').height();
       $(val).find('iframe').attr('height', height);
    });
  }
};

$(document).ready(function ($) {
  if ($('.page-polos')) {
    var select2Polos = $('#filter-polos').select2();

    select2Polos.on('open', function (e) {
      this.dropdown._resizeDropdown();
      this.dropdown._positionDropdown();
    });

    $("#filter-polos").change(function(event) {
      var value = event.target.value;

      if (value === 'all') {
        $(".container-polos").fadeOut();
      } else {
        $(".panel-heading-polo:contains('"+value+"')").parent().fadeIn();
        $(".panel-heading-polo:contains('"+value+"')").parent().parent().fadeIn();
        $(".panel-heading-polo:not(:contains('"+value+"'))").parent().fadeOut();
        $(".panel-heading-polo:not(:contains('"+value+"'))").parent().parent().fadeOut();
        fixMapsPolos($);
      }

    });
  }
});
