function validateTab (index) {
  fv = $('.pre-enrollment-form').data('formValidation'), // FormValidation instance
    // The current tab
  $tab = $('.pre-enrollment-form').find('.tab-pane').eq(index)
  // Validate the container
  fv.validateContainer($tab)
  var isValidStep = fv.isValidContainer($tab)
  if (isValidStep === false || isValidStep === null) {
    // Do not jump to the target tab
    return false
  }
  return true
}

function wizardButtonsNamePreEnrollment (index) {
  switch (index) {
    case 0:
      $('.pre-enrollment-form .previous').addClass('disabled')
      $('.pre-enrollment-form .next').removeClass('pre-enrollment-form-submit').text('Ótimo! Próximo passo')
      break
    case 1:
      $('.pre-enrollment-form .previous').removeClass('disabled')
      $('.pre-enrollment-form .next').removeClass('disabled').text('Realizar pré matrícula')
      break
    default:
      break
  }
}

$(document).ready(function ($) {
  $('.pre-enrollment-form').formValidation({
    framework: 'bootstrap',
    locale: 'pt_BR',
    fields: {
      email: {
        validators: {
          emailAddress: {
            message: 'Insira um e-mail válido'
          }
        }
      }
    }
  })

  $('.pre-enrollment-form').on('success.form.fv', function (event) {
    event.preventDefault()
    $('.btn-primary').append('<span class="icon-spinner8 rotating"></span>');
    const data = $(this).serializeObject();
    $.post(baseUrl() + 'wp-admin/admin-ajax.php', {action: 'send_pre_enrollment', data: data}, function (response) {
      $('.btn-primary span').remove();
      window.location.href = response.data.redirect;
    })
  })

  $('.pre-enrollment-form').bootstrapWizard({
    tabClass: 'nav nav-tabs',
    nextSelector: '.btn-primary.next',
    previousSelector: '.btn-default.previous',
    onTabClick: function (tab, navigation, index, next) {
      if (index > next) {
        return true
      } else {
        return validateTab(index)
      }
    },
    onNext: function (tab, navigation, index) {
      var numTabs = $('.pre-enrollment-form .nav-tabs li').length

      if (!validateTab(index - 1)) {
        return false
      }
      if (index === (numTabs)) {
        // We are at the last tab
        // Uncomment the following line to submit the form using the defaultSubmit() method
        if (validateTab(index)) {
          $('.pre-enrollment-form').submit()
        }
        // For testing purpose
      }
      return true
    },
    onTabShow: function (tab, navigation, index) {
      wizardButtonsNamePreEnrollment(index)
    }
  })

  if ($('.pre-enrollment-form').length > 0) {
    $.post(baseUrl() + 'wp-admin/admin-ajax.php', {action: 'get_ensino'}, function (response) {
      $('select.tipo-curso').html('<option selected disabled>Selecione um tipo de curso</option>')
      $.each(response.data, function (index, val) {
        $('select.tipo-curso').append('<option value="' + val.ID + '">' + val.post_title + '</option>')
      })
    })
  }

  $('select.tipo-curso').change(function (event) {
    $.post(baseUrl() + 'wp-admin/admin-ajax.php', {action: 'get_cursos', id: parseInt(event.target.value)}, function (response) {
      response.data.sort(function (a, b) {
        if (a.post_title < b.post_title) { return -1 }
        if (a.post_title > b.post_title) { return 1 }
      })
      $('select.cursos').html('<option selected disabled>Selecione um curso</option>')
      $.each(response.data, function (index, val) {
        $('select.cursos').append('<option value="' + val.ID + '">' + val.post_title + '</option>')
      })
    })
  })
})
