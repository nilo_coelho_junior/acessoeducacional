<h2 class="text-center"><strong>Agora, fale um pouquinho do imóvel</strong></h2>
<br>

<div class="form-group">
  <label for="oImovelEncontraSeDentroDeCondominio" class="col-sm-6 control-label">O Imóvel encontra-se dentro de Condomínio?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="oImovelEncontraSeDentroDeCondominio" id="oImovelEncontraSeDentroDeCondominio" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="oImovelEncontraSeDentroDeCondominio" id="oImovelEncontraSeDentroDeCondominio" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group tipoDeCondominio hide">
  <label for="tipoDeCondominio" class="col-sm-6 control-label">Tipo do Condomínio</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="tipoDeCondominio" id="tipoDeCondominio" value="Condomínio de casas aberto" required> Condomínio de casas aberto
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="tipoDeCondominio" id="tipoDeCondominio" value="Condomínio de casas fechado sem vigilância permanente"> Condomínio de casas fechado sem vigilância permanente
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="tipoDeCondominio" id="tipoDeCondominio" value="Condomínio de casas fechado com vigilância permanente"> Condomínio de casas fechado com vigilância permanente
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="terrenoBaldioNoFundoOuLateralDoImovel" class="col-sm-6 control-label">Existe terreno baldio nos fundos e/ou nas laterais do imóvel?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="terrenoBaldioNoFundoOuLateralDoImovel" id="terrenoBaldioNoFundoOuLateralDoImovel" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="terrenoBaldioNoFundoOuLateralDoImovel" id="terrenoBaldioNoFundoOuLateralDoImovel" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="imovelDesabitadoDesocupadoOuEmContrucao" class="col-sm-6 control-label">O imóvel está desabitado, desocupado ou em construção?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="imovelDesabitadoDesocupadoOuEmContrucao" id="imovelDesabitadoDesocupadoOuEmContrucao" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="imovelDesabitadoDesocupadoOuEmContrucao" id="imovelDesabitadoDesocupadoOuEmContrucao" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="possuiCofreEmbutido" class="col-sm-6 control-label">Possui cofre embutido?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="possuiCofreEmbutido" id="possuiCofreEmbutido" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiCofreEmbutido" id="possuiCofreEmbutido" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="possuiSistemaDeAlarmeContraRoubo" class="col-sm-6 control-label">Possui sistema de alarme contra roubo?</label>
  <div class="col-sm-3">
    <select class="form-control" name="possuiSistemaDeAlarmeContraRoubo" required>
    	<option selected disabled>Selecione uma opção</option>
			<option value="Não possui alarme">Não possui alarme</option>
			<option value="Sim, com monitoramento de empresa de segurança especializada">Sim, com monitoramento de empresa de segurança especializada</option>
			<option value="Sim, sem monitoramento de empresa de segurança especializada">Sim, sem monitoramento de empresa de segurança especializada</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label for="janelasProtegidasPorGrade" class="col-sm-6 control-label">As janelas são protegidas por grades?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="janelasProtegidasPorGrade" id="janelasProtegidasPorGrade" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="janelasProtegidasPorGrade" id="janelasProtegidasPorGrade" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="residenciaLocalizadaNaZonaRural" class="col-sm-6 control-label">A residência está localizada em zona rural?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="residenciaLocalizadaNaZonaRural" id="residenciaLocalizadaNaZonaRural" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="residenciaLocalizadaNaZonaRural" id="residenciaLocalizadaNaZonaRural" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="atividadeComercialNoImovel" class="col-sm-6 control-label">Você pratica atividade comercial neste imóvel?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="atividadeComercialNoImovel" id="atividadeComercialNoImovel" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="atividadeComercialNoImovel" id="atividadeComercialNoImovel" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group desejaSeguroParaEquipamentos hide">
  <label for="desejaSeguroParaEquipamentos" class="col-sm-6 control-label">Deseja contratar seguro para equipamentos, móveis, materiais e utensílios, utilizados na atividade comercial dentro da residência a ser segurada?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="desejaSeguroParaEquipamentos" id="desejaSeguroParaEquipamentos" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="desejaSeguroParaEquipamentos" id="desejaSeguroParaEquipamentos" value="Não"> Não
			</label>
		</div>
  </div>
</div>