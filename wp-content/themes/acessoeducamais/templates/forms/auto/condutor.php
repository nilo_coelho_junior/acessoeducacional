<h2 class="text-center"><strong>Quem é o principal condutor?</strong></h2>
<h4 class="text-center"><i>O principal condutor dirige por, pelo menos, 85% do tempo de uso do veículo</i></h4>
<input type="hidden" name="section" value="Quem é o principal condutor?">
<br>
<div class="form-group">
  <label for="nomeCondutor" class="col-sm-6 control-label">Nome Completo</label>
  <div class="col-sm-4">
		<input type="text" class="form-control" name="nomeCondutor" id="nomeCondutor" placeholder="Nome Completo sem abreviações" required>
  </div>
</div>
<div class="form-group">
  <label for="cpfCondutor" class="col-sm-6 control-label">CPF do Condutor</label>
  <div class="col-sm-4">
		<input type="text" data-mask="cpf" class="form-control" name="cpfCondutor" id="cpfCondutor" placeholder="" required>
  </div>
</div>
<div class="form-group">
  <label for="sexoCondutor" class="col-sm-6 control-label">Sexo</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="sexoCondutor" id="sexoCondutor" value="Masculino" required> Masculino
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="sexoCondutor" id="sexoCondutor" value="Feminino"> Feminino
			</label>
		</div>
  </div>
</div>
<div class="form-group dataNascimentoCondutor">
  <label for="dataNascimentoCondutor" class="col-sm-6 control-label">Data de Nascimento</label>
  <div class="col-sm-4">
		<input type="text" data-mask='date' class="form-control" name="dataNascimentoCondutor" id="dataNascimentoCondutor" placeholder="dd/mm/aaaa" required>
  </div>
</div>
<div class="form-group">
  <label for="estadoCivilCondutor" class="col-sm-6 control-label">Estado Civil</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="estadoCivilCondutor" id="estadoCivilCondutor" value="Casado(a) ou reside há pelo menos 2 anos com companheiro(a)" required> Casado(a) ou reside há pelo menos 2 anos com companheiro(a)
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="estadoCivilCondutor" id="estadoCivilCondutor" value="Separado(a)/Divorciado(a)"> Separado(a)/Divorciado(a)
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="estadoCivilCondutor" id="estadoCivilCondutor" value="Solteiro(a)"> Solteiro(a)
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="estadoCivilCondutor" id="estadoCivilCondutor" value="Viúvo(a)"> Viúvo(a)
			</label>
		</div>
  </div>
</div>
<div class="form-group">
  <label for="profissaoCondutor" class="col-sm-6 control-label">Qual é a profissão do principal condutor? </label>
  <div class="col-sm-4">
		<input type="text" class="form-control" name="profissaoCondutor" id="profissaoCondutor" placeholder="Digite a profissão" required>
  </div>
</div>
<div class="form-group hide anoPrimeiraHabilitacaoCondutor">
  <label for="anoPrimeiraHabilitacaoCondutor" class="col-sm-6 control-label">Ano da primeira habilitação?</label>
  <div class="col-sm-4">
    <select class="form-control anoPrimeiraHabilitacaoCondutor" name="anoPrimeiraHabilitacao" required>
    	<option>18</option>
    </select>
  </div>
</div>
<div class="form-group">
  <label for="moradiaCondutor" class="col-sm-6 control-label">O principal condutor mora em:</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="moradiaCondutor" id="moradiaCondutor" value="Apartamento ou flat" required> Apartamento ou flat
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="moradiaCondutor" id="moradiaCondutor" value="Casa em condominio fechado"> Casa em condominio fechado
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="moradiaCondutor" id="moradiaCondutor" value="Casa ou sobrado"> Casa ou sobrado
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="moradiaCondutor" id="moradiaCondutor" value="Chácara, fazenda ou sítio"> Chácara, fazenda ou sítio
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="moradiaCondutor" id="moradiaCondutor" value="Outros"> Outros
			</label>
		</div>
  </div>
</div>

