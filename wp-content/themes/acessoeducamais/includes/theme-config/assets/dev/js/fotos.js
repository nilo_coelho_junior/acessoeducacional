$(document).ready(function ($) {
  if ($('.single-fotos')) {
    $('#gallery-main').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '#gallery-thumb'
    });
    $('#gallery-thumb').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '#gallery-main',
      dots: true,
      centerMode: true,
      focusOnSelect: true
    });
  }
});
