<?php 

global $parent;

?>

<!-- Nav tabs -->
<ul class="nav nav-tabs steps hide-for-small-only hidden" role="tablist">
  <li role="presentation" class="active">
  	<a href="#first" aria-controls="first" role="tab" data-toggle="tab">
      	<span class="visible-sm visible-xs">1</span>
      	<span class="hidden-sm hidden-xs">Informe os dados da sua empresa</span>
  	</a>
  </li>
</ul>
<!-- Tab panes -->
<?php get_template_part('templates/loading'); ?>
<div class="tab-content container">
  <div role="tabpanel" class="tab-pane active" id="first">
  	<?php get_template_part('templates/forms/'.$parent->post_name.'/contato'); ?>
  </div>
  <div class="container">
		<div class="col-lg-12 text-center">
			<button class="btn btn-primary btn-lg next">Solicitar cotação</button>
		</div>
	</div>
</div>