<div class="col-lg-4">
  <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
    <div id="widget-area" class="widget-area" role="complementary">
      <?php dynamic_sidebar( 'sidebar' ); ?>
    </div><!-- .widget-area -->
  <?php endif; ?>
</div>
