<?php get_header(); ?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title"><?php the_title(); ?></h2>
  </div>

  <div class="container">
    <?php
      if ( have_posts() ) : while ( have_posts() ) : the_post();
        echo the_content();
      endwhile; endif;
    ?>
  </div>
  <br>

  <div class="container">

    <?php
      $depoimentos = new WP_Query(array('post_type' => 'depoimento'));

      while ( $depoimentos->have_posts() ) : $depoimentos->the_post();
        $ensino_id = get_post_meta(get_the_ID(), '_wpcf_belongs_ensino_id', true);
        $ensino = get_post($ensino_id);
        $curso_id = get_post_meta(get_the_ID(), '_wpcf_belongs_curso_id', true);
        $curso = get_post($curso_id);
    ?>
      <div class="media media-img-small media-well">
        <div class="media-left">
          <?php the_post_thumbnail('thumbnail', ['class' => 'media-object img-circle']); ?>
        </div>
        <div class="media-body">
          <h3 class="media-heading page-title">
            <strong>
              <?php echo the_title(); ?>,
              <?php echo $ensino->post_title; ?> -
              <?php echo $curso->post_title; ?>
            </strong>
          </h3>
          <br>
          <?php echo the_content(); ?>
        </div>
      </div>
    <?php
      endwhile;
    ?>
    </div>
    <?php if ( $depoimentos->max_num_pages > 1 ) : ?>
      <nav aria-label="Page navigation">
        <ul class="pagination col-sm-12">
          <li class="pull-left">
            <?php previous_posts_link('<span class="meta-nav">&laquo;</span> Posts Recentes'); ?>
          </li>
          <li class="pull-right">
            <?php next_posts_link('Posts antigos <span class="meta-nav">&raquo;</span>', 0); ?>
          </li>
        </ul>
      </nav>
    <?php endif; ?>
  </div>

</div>

<?php get_footer(); ?>
