<?php get_header(); ?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title text-center"><?php the_title(); ?></h2>
  </div>

  <div class="container text-center">
    <span class="icon-check-circle icon-xlg"></span>
    <h3>
      <?php
        if ( have_posts() ) : while ( have_posts() ) : the_post();
          echo the_content();
        endwhile; endif;
      ?>
    </h3>
    <br>
    <a href="<?php echo get_permalink( get_option( 'page_for_posts' )) ?>" class="btn btn-primary btn-lg">
      Conheça nosso blog!
    </a>
  </div>
</div>

<?php get_footer(); ?>
