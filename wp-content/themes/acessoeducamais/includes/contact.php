<div class="container container-wrapper">
  <h2 class="page-title text-center">Fale Conosco</h2>
  <div class="col-sm-6">
    <?php
      $iframe = htmlspecialchars(get_option('maps_iframe'));
      $pieces = explode("=&quot;", $iframe);
      $url_pieces = explode("&quot;", $pieces[1]);
      $url = $url_pieces[0];
    ?>
    <iframe src="<?php echo $url; ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
  <div class="col-sm-6">
    <?php echo do_shortcode('[contact-form-7 id="288" title="Contato"]'); ?>
  </div>
</div>
