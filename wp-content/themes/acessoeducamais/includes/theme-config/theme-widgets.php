<?php // Register and load the widget
function aedu_load_widget() {
  register_widget( 'aedu_widget' );
}
add_action( 'widgets_init', 'aedu_load_widget' );

// Creating the widget
class aedu_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      // Base ID of your widget
      'aedu_widget',
      // Widget name will appear in UI
      __('Últimas Notícias', 'aedu_widget_domain'),
      // Widget description
      array( 'description' => __( 'Exibe as últimas notícias', 'aedu_widget_domain' ), )
    );
  }

  // Creating widget front-end

  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
    echo $args['before_title'] . $title . $args['after_title'];

    // This is where you run the code and display the output
    $query = new WP_Query(array('post_type' => 'post', 'orderby' => 'date', 'posts_per_page' => 5));
    if ($query->have_posts()):
    ?>
      <div class="widget widget-recent-posts">
        <div class="aedu-widget-post">
          <?php while($query->have_posts()) : $query->the_post(); ?>
              <div class="aedu-widget-post-item">
                <a href="<?php the_permalink(); ?>">
                  <div class="col-sm-3 post-image no-pd-l">
                    <img src="<?php the_post_thumbnail_url('thumbnail');?>" alt="">
                  </div>
                  <div class="col-sm-9 post-title-wrapper no-pd-l">
                    <h4 class="post-title"><?php the_title(); ?></h4>
                  </div>
                </a>
              </div>
          <?php endwhile; ?>
        </div>
      </div>
    <?php
    wp_reset_query();
    endif;

    echo $args['after_widget'];
  }

  // Widget Backend
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
    }
    else {
      $title = __( 'Últimas Notícias', 'aedu_widget_domain' );
    }
    // Widget admin form
    ?>
      <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
      </p>
    <?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
  }
} // Class aedu_widget ends here

?>
