<ul class="nav nav-pills nav-stacked tipodecurso-nav col-sm-3" role="tablist">
  <?php if ($nav_ensino_selected == 'curso') { ?>
    <li role="presentation" class="active">
      <a href="<?php echo get_permalink( $ensino ); ?>">Cursos</a>
    </li>
  <?php } else { ?>
    <li role="presentation" class="<?php echo $nav_ensino_selected == 'cursos' ? 'active' : '';?>">
      <a href="#cursos" id="cursos-tab" role="tab" data-toggle="tab" aria-controls="cursos" aria-expanded="true">Cursos</a>
    </li>
  <?php } ?>
  <li role="presentation" class="<?php echo $nav_ensino_selected == 'faculdade' ? 'active' : '';?>">
    <a href="#faculdade" role="tab" id="faculdade-tab" data-toggle="tab" aria-controls="faculdade" aria-expanded="false">Tudo sobre a Faculdade</a>
  </li>
  <li role="presentation" class="<?php echo $nav_ensino_selected == 'faq' ? 'active' : '';?>">
    <a href="#faq" role="tab" id="faq-tab" data-toggle="tab" aria-controls="faq" aria-expanded="false">Perguntas Frequentes</a>
  </li>
</ul>
