<!doctype html>
<html class="no-js" lang="pt-BR" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="content-language" content="pt-br" />
    <title><?php bloginfo('name'); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/dist/img/favicon.png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <!-- <div class="load-page animated fadeIn">
      // <?php // get_template_part('templates/loading' ); ?>
    </div> -->
    <div class="main-page">
      <div class="pre-enrollment">
        <ul class="pre-enrollment-list container">
          <li class="col-sm-2">
            <span class="icon-whatsapp-logo"></span>&nbsp;<?php echo get_option('whatsapp'); ?>
          </li>
          <li class="col-sm-2">
            <span class="icon-phone"></span>&nbsp;<?php echo get_option('contact_number'); ?>
          </li>
          <li class="col-sm-5">
            <?php echo get_option('office_hours'); ?>
          </li>
          <li class="col-sm-3">
            <a href="<?php echo get_permalink(get_page_by_path('pre-matricula')); ?>" class="btn btn-amaranth">Faça sua Pré-Matrícula!</a>
          </li>
        </ul>
      </div>
      <nav class="navbar navbar-default" id="navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo home_url(); ?>">
              <img src="<?php header_image(); ?>" alt="" />
            </a>
          </div>

          <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
            <?php
              wp_nav_menu( array(
                'menu'              => 'main-menu',
                'theme_location'    => 'main-menu',
                'depth'             => 3,
                'container'         => 'false',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker())
              );
            ?>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo get_option('facebook_url' ); ?>" target="_blank"><span class="icon-facebook"></span></a></li>
              <li><a href="<?php echo get_option('instagram_url' ); ?>" target="_blank"><span class="icon-instagram"></span></a></li>
            </ul>
          </div>

          <!-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="<?php //echo home_url(); ?>">Home<span class="sr-only">(current)</span></a></li>
              <li><a href="http://acessoeducacional.com/index.php?pg=empresa">Institucional</a></li>
              <li><a href="http://acessoeducacional.com/index.php?pg=especializacao">Especializações</a></li>
              <li><a href="http://acessoeducacional.com/index.php?pg=mestrados">Mestrados</a></li>
              <li><a href="http://acessoeducacional.com/index.php?pg=doutorados">Doutorados</a></li>
              <li><a href="http://acessoeducacional.com/index.php?pg=calendario">Calendário</a></li>
              <li><a href="http://acessoeducacional.com/index.php?pg=polos">Polos</a></li>
              <li><a href="http://acessoeducacional.com/index.php?pg=galeria">Fotos</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php //echo get_option('facebook_url' ); ?>" target="_blank"><span class="icon-facebook"></span></a></li>
              <li><a href="<?php //echo get_option('instagram_url' ); ?>" target="_blank"><span class="icon-instagram"></span></a></li>
            </ul>
          </div> -->
        </div>
      </nav>
      <?php if (!is_front_page()) { ?>
        <div class="container-fluid" typeof="BreadcrumbList" vocab="http://schema.org/">
          <div class="container breadcrumb-navxt">
            <?php if(function_exists('bcn_display')) {
              bcn_display();
            }?>
          </div>
        </div>
      <?php } ?>
