function wizardButtonsNameVida(index){
	switch (index) {
		case 0:
			$('.cotacao .previous').addClass('disabled')
			$('.cotacao .next').removeClass('disabled')
			break;
		case 1:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').text("Solicitar Cotação")
			break;
		default:
			break;
	}
}

$(function() {

	$(".cotacao.vida").formValidation({
		framework: "bootstrap",
		locale: "pt_BR",
		fields: {
			cep: {
				validators: {
					zipCode:{
						country: "BR"
					}
				}
			},
			cpf: {
				validators:{
					callback:{
						message: "Cpf inválido",
						callback: function (value, validator, $field) {
							var cpf = value.replace(".", "")
							cpf = cpf.replace(".", "")
							cpf = cpf.replace("-", "");
							return CPF.validate(cpf)
						}
					}
				}
			}
		}
	})
});