<?php
require_once('includes/theme-config/theme-config.php');

add_action( 'wp_ajax_get_ensino', 'get_ensino' );
add_action( 'wp_ajax_nopriv_get_ensino', 'get_ensino' );

function get_ensino() {
  $query = get_posts(array('post_type' => 'ensino', 'orderby' => 'menu_order', 'order' => 'ASC'));
  wp_send_json_success($query);
}

add_action( 'wp_ajax_get_cursos', 'get_cursos' );
add_action( 'wp_ajax_nopriv_get_cursos', 'get_cursos' );

function get_cursos() {
  $query = new WP_Query(array('post_type' => 'curso', 'meta_query' => array(array('key' => '_wpcf_belongs_ensino_id', 'value' => $_POST[id])), 'orderby' => 'title', 'order' => 'ASC'));
  wp_send_json_success($query->posts);
}

add_action( 'wp_ajax_send_pre_enrollment', 'send_pre_enrollment' );
add_action( 'wp_ajax_nopriv_send_pre_enrollment', 'send_pre_enrollment' );

function send_pre_enrollment() {
  $data = $_POST['data'];
  // return mailster_lists_by_course($data['ensino'], $data['curso']);
  if(!wp_verify_nonce( $data['token'], 'send_pre_enrollment' )){
    wp_send_json_error("Desculpe, algo deu errado. Por favor recarrega a página e tente novamente");
  }else{
    $post_data = array(
      'post_author' => 1,
      'post_title' => $data['name'],
      'post_status' => 'publish',
      'post_type' => 'post-pre-matricula',
      'post_date' => current_time('mysql')
    );

    $post_id = wp_insert_post($post_data);

    if (!empty($data['phoneType'])){
      update_post_meta( $post_id, "wpcf-tipo", $data['phoneType']);
    }

    if (!empty($data['city'])){
      update_post_meta( $post_id, "wpcf-cidade", $data['city']);
    }

    if (!empty($data['phone'])){
      update_post_meta( $post_id, "wpcf-telefone-para-contato", $data['phone']);
    }

    if (!empty($data['state'])){
      update_post_meta( $post_id, "_wpcf_belongs_estado_id", $data['state']);
    }

    if (!empty($data['ensino'])){
      update_post_meta( $post_id, "_wpcf_belongs_ensino_id", $data['ensino']);
    }

    if (!empty($data['curso'])){
      update_post_meta( $post_id, "_wpcf_belongs_curso_id", $data['curso']);
    }

    if (!empty($data['email'])){
      update_post_meta( $post_id, "wpcf-email", $data['email']);
    }

    if(function_exists('mailster')){
      //prepare the userdata from a $_POST request. only the email is required
      $firstname = explode(" ", $data['name'])[0];
      $array_name = explode(" ", $data['name']);
      array_shift($array_name);
      $lastname = implode(" ", $array_name);
      $userdata = array(
        'email' => $data['email'],
        'firstname' => $firstname,
        'lastname' => $lastname,
        'status' => 1, //0 forces a confirmation message
      );

      $overwrite = true;

      //add a new subscriber and $overwrite it if exists
      $subscriber_id = mailster('subscribers')->add($userdata, $overwrite);

      //if result isn't a WP_error assign the lists
      if(!is_wp_error($subscriber_id)){
        update_post_meta( $post_id, "wpcf-subscriber-id", $subscriber_id);
        //your list ids
        $lists = mailster_lists_by_course($data['ensino'], $data['curso']);
        mailster('subscribers')->assign_lists($subscriber_id, $lists);
      }
    }
    $data['redirect'] = get_permalink(get_page_by_path('confirmacao-pre-matricula'));
    $ensino = get_post(intval($data['ensino']));
    $mailster_action = "mailster_welcome_".$ensino->post_name;
    error_log($ensino->post_name);
    error_log($ensino->post_name == 'graduacao');
    error_log($ensino->post_name == 'especializacao');
    error_log($ensino->post_name == 'mestrado');
    error_log($ensino->post_name == 'doutorado');
    if ($ensino->post_name == 'graduacao') {
      do_action( "mailster_welcome_graduacao", $subscriber_id );
    } else if ($ensino->post_name == 'especializacao') {
      do_action( "mailster_welcome_especializacao", $subscriber_id );
    } else if ($ensino->post_name == 'mestrado') {
      do_action( "mailster_welcome_mestrado", $subscriber_id );
    } else if ($ensino->post_name == 'doutorado') {
      do_action( "mailster_welcome_doutorado", $subscriber_id );
    }
    wp_send_json_success($data);
  }
}

function mailster_lists_by_course($typeCourse_id, $course_id) {
  $all_lists = mailster('lists')->get();
  $typeCourse = get_post(intval($typeCourse_id));
  $course = get_post(intval($course_id));
  $list = [];
  foreach ($all_lists as $key => $value) {
    if ($value->name == 'Default List' || $value->name == $course->post_title || $value->name == $typeCourse->post_title){
      array_push($list, $value->ID);
    }
  }
  return $list;
}

function get_curso_field($name, $curso_id, $category_id) {
  $field_tipo = get_term_meta( $category_id, 'wpcf-'.$name.'-tipo', true );
  $field = get_post_meta($curso_id, 'wpcf-'.$name, true);

  if (empty($field)) {
    return $field_tipo;
  } else {
    return $field;
  }
}

function get_curso_field_metodologia($curso_id, $category_id) {
  $metodologia_em_texto = get_post_meta($curso_id, 'wpcf-metodologia-em-texto', true);

  if ($metodologia_em_texto) {
    $metodologia_texto_tipo = get_term_meta( $category_id, 'wpcf-metodologia-texto-tipo', true );
    $metodologia_texto = get_post_meta($curso_id, 'wpcf-metodologia-texto', true);
    if (empty($metodologia_texto)){
      return ['text', $metodologia_texto_tipo];
    } else {
      return ['text', $metodologia_texto];
    }
  } else {
    $metodologia_tipo = get_term_meta( $category_id, 'wpcf-metodologia-tipo', true );
    $metodologia = get_post_meta($curso_id, 'wpcf-metodologia', true);
    if (empty($metodologia)) {
      return ['pdf', $metodologia_tipo];
    } else {
      return ['pdf', $metodologia];
    }
  }
}

?>
