<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'acessoeducamais');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O1(Gq*#i.q]`KXg$c;U;S8B.*+)C;3m+ChUpqVzJ]Qs6YWrx3IWR2.&/w5Df<#zv');
define('SECURE_AUTH_KEY',  'oHkM0b:l63h%LPCPzm=u(H R#RY>@Z`dC`feWDj_ll< 0tgM(p~[ XFRmG4M->>@');
define('LOGGED_IN_KEY',    'z-}^K}(wFY4@BxesfC;(/r]QXTEa9P1]0F<4I6gE,)GT 0l*hQT9KCCqH,3Y&VwV');
define('NONCE_KEY',        'hULWxbagYqZ}^9Eq6Kx{`rm9=pCeoEHWIxG.q!l]Fjrr~PE|a8)IJZD0]kRX=U;r');
define('AUTH_SALT',        '_b/%coy37{-DR@O^`I|4mc1M=z%H(p+_nYST^KM&_<4HpA4B{Z0}oxkD+Ew(&F3Q');
define('SECURE_AUTH_SALT', '@6HE,aGTiR,}]6Mk6Q<nchd^m*MBEhR&){~B_6uOEd`0Mc()1s]Jb2rDA8m6k/2b');
define('LOGGED_IN_SALT',   'x2U7/%NJarRhgnSx$}:o+%1G3fa)(JH$U#ro?MxsZ%X&}&Vkxu)5*OAQZI:w(&4S');
define('NONCE_SALT',       '!&* eP2:Qd.E)6HJ`q=j(e|~5IYpN]nIq/H5%yU;@IaqkBXI;86k?F,.lCIc_#29');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', true);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
