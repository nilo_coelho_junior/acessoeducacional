<?php get_header() ?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title">Polo <?php echo the_title(); ?></h2>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-6 polo-informacoes">
        <!-- <h4><strong>Informações</strong></h4> -->
        <!-- <hr> -->
        <br>
        <p>
          Início das aulas: <br>
          <strong><?php echo date_i18n('d \d\e F \d\e Y', get_post_meta(get_the_ID(), 'wpcf-inicio-das-aulas', true)); ?></strong>
        </p>

        <p>
          Período de Matrículas: <br>
          <strong><?php echo get_post_meta(get_the_ID(), 'wpcf-periodo-de-matriculas-abertas', true); ?></strong>
        </p>

        <?php
          $email = get_post_meta(get_the_ID(), 'wpcf-polo-e-mail', true);
          if (!empty($email)) {
        ?>
          <p>
            E-mail: <br>
            <strong><?php echo $email; ?></strong>
          </p>
        <?php } ?>

        <?php
          $telefones = get_post_meta(get_the_ID(), 'wpcf-telefones', true);
          if (!empty($telefones)) {
        ?>
          <p>
            Telefones: <br>
            <strong><?php echo $telefones; ?></strong>
          </p>
        <?php } ?>
        <?php echo nl2br(get_post_meta(get_the_ID(), 'wpcf-endereco-polo', true)); ?>
        <br><br>
      </div>
      <div class="col-sm-6 no-pd">
        <?php
          $iframe = get_post_meta( get_the_ID(), 'wpcf-google-maps-iframe', true );
          $url = explode('" width="', explode('src="', $iframe)[1])[0];
        ?>
        <iframe src="<?php echo $url; ?>" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="col-sm-12">
        <hr>
        <h4><strong>Eventos</strong></h4>
        <br>
        <?php
          $eventos = new WP_Query(array('post_type' => 'evento', 'meta_query' => array(array('key' => '_wpcf_belongs_polo_id', 'value' => get_the_ID()))));
          if ( !$eventos->have_posts() ) {
        ?>
            <p>Não exite eventos para o polo.</p>
        <?php
          } else {
        ?>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Data</th>
                  <th>Detalhes</th>
                </tr>
              </thead>
              <tbody>
        <?php
            while ( $eventos->have_posts() ) : $eventos->the_post();
              $date_start = date_i18n('d/m/Y', get_post_meta(get_the_ID(), 'wpcf-data-de-inicio-do-evento', true));
              $date_end = date_i18n('d/m/Y', get_post_meta(get_the_ID(), 'wpcf-data-de-encerramento-do-evento', true));
              $date = $date_start;
              if (!empty($date_end) && $date_end != $date_start){
                $date = 'de: '.$date.' até '.$date_end;
              }
        ?>
              <tr>
                <th><?php echo $date; ?></th>
                <td>
                  <?php echo the_title(); ?>
                  <br><br>
                  <?php echo the_content(); ?>
                </td>
              </tr>
        <?php
            endwhile;
        ?>
              </tbody>
            </table>
        <?php
          }
        ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
