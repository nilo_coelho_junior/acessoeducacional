function wizardButtonsNameAuto(index){
	switch (index) {
		case 0:
			$('.cotacao .previous').addClass('disabled')
			break;
		case 1:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').text("Entendi, vamos em frente")
			break;
		case 2:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').text("Proseguindo...")
			break;
		case 3:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').text("Só mais um detalhe")
			break;
		case 4:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').removeClass('disabled').text("Ufa! Terminamos")
			break;
		default:
			break;
	}
}

$(function() {

	$("select.marcas").change(function(){
		var marca = $(this).find("option:selected").data('value');
		$("select.modelos").html('<option>Carregando...</option>');
		$.post(baseUrl()+"wp-admin/admin-ajax.php", {action : 'get_modelos', marca: marca}, function(data){
			data = JSON.parse(data);
			$("select.modelos").html('<option selected disabled>Selecione uma marca</option>');
			$.each(data.modelos, function(index, val) {
				$("select.modelos").append('<option value="'+val.nome+'" data-value="'+val.codigo+'">'+val.nome+'</option>')
			});		
		});
	});

	$("select.modelos").change(function(){
		var marca = $("select.marcas").find(":selected").data('value');
		var modelo = $(this);
		if (modelo.val().split("-")[0] == new Date().getFullYear()){
			$(".form-group.0km").toggleClass('hide');
		}

		$("select.anos").html('<option>Carregando...</option>');
		$.post(baseUrl()+"wp-admin/admin-ajax.php", {action : 'get_anos', marca: marca, modelo: modelo.find("option:selected").data('value')}, function(data){
			data = JSON.parse(data);
			$("select.anos").html('<option selected disabled>Selecione um modelo</option>');
			$.each(data, function(index, val) {
				$("select.anos").append('<option value="'+val.nome+'" data-value="'+val.codigo+'">'+val.nome+'</option>')
			});
		});
	});

	$.post(baseUrl()+"wp-admin/admin-ajax.php", {action : 'get_marcas'}, function(data){
		data = JSON.parse(data);
		$("select.marcas").html('<option selected disabled>Selecione uma marca</option>');
		$.each(data, function(index, val) {
			$("select.marcas").append('<option value="'+val.nome+'" data-value="'+val.codigo+'">'+val.nome+'</option>')
		});
	});

	$("input[name='nomeCompleto']").keyup(function(event) {
		principalCondutor = $("#ehPrincipalCondutor").val();
		
		if (principalCondutor == "Sim"){
			$("input[name='nomeCondutor']").val(event.target.value);
			$("input[name='nomeCompletoSegurado']").val(event.target.value);
			$("input[name='nomeCompletoContato']").val(event.target.value);
		}
	});

	$("input[name='ehPrincipalCondutor']").change(function(event) {
		if (event.target.value == "Sim") {
			$(".cotacao").bootstrapWizard('hide', 2);
			resizeSteps();
		}else{
			$(".cotacao").bootstrapWizard('display', 2);
			resizeSteps();
		}
	});

	$("input[name='sexo']").change(function(event) {
		if (event.target.value == "Masculino") {
			$("input[name='sexoCondutor'][value='Masculino']").trigger("click");
			$("input[name='sexoContato'][value='Masculino']").trigger("click");
		} else {
			$("input[name='sexoCondutor'][value='Feminino']").trigger("click");
			$("input[name='sexoContato'][value='Feminino']").trigger("click");
		}
	});

	$("input[name='dataNascimentoCondutor']").keyup(function(event) {
		$("input[name='dataNascimentoContato']").val(event.target.value);
	});

	var agePeopleDriverValidatiors = {
		row: ".col-sm-4",
		validators: {
			notEmpty: {
				message: "Por favor insira um valor"
			},
			between: {
          min: 17,
          max: 100,
          message: 'Idade permitida deve estar entre 17 e 100 anos'
      }
		}
	}

	$(".cotacao.auto").formValidation({
		framework: "bootstrap",
		locale: "pt_BR",
		fields: {
			nomeCompleto: {
				validators: {
					notEmpty: {
						message: "Insira seu nome completo"
					}
				}
			},
			incluirKitGas: {
				enabled: false
			},
			incluirKitGasValor: {
				enabled: false
			},
			taxiIsencaoImposto: {
				enabled: false
			},
			quaisDispositivosAntifurto: {
				enabled: false,
				validators:{
					notEmpty: {
						message: 'Escolha pelo menos uma opção dos dispositivos antifurto'
					}
				}
			},
			dataNascimento: {
				validators:{
					date:{
						message: "O condutor deve ter 18 anos ou mais",
						format: "DD/MM/YYYY",
						min: minAge(),
						max: maxAge()
					}
				}
			},
			possuiGaragemFaculdade: {
				enabled: false
			},
			distanciaResidenciaTrabalho: {
				enabled: false
			},
			possuiGaragemTrabalho: {
				enabled: false
			},
			mesmoModeloDaApoliceAnterior: {
				enabled: false
			},
			seguradoraAnterior: {
				enabled: false
			},
			vencimentoDaApolice: {
				enabled: false
			},
			classeBonusApoliceAnterior: {
				enabled: false
			},
			quantosSinistrosOcorreram: {
				enabled: false
			},
			bancoQueTemConta: {
				enabled: false
			},
			cepEstacionadoNoite: {
				validators: {
					zipCode:{
						country: "BR"
					}
				}
			},
			cpfSegurado: {
				validators:{
					callback:{
						message: "Cpf inválido",
						callback: function (value, validator, $field) {
							var cpf = value.replace(".", "")
							cpf = cpf.replace(".", "")
							cpf = cpf.replace("-", "");
							return CPF.validate(cpf)
						}
					}
				}
			}
		}
	}).on('change', '[name="possuiAlgumDispositivoAntifurto"]', function(e){
		var fv = $(".cotacao.auto").data("formValidation");
		var possuiAlgumDispositivoAntifurto = $("input[name='possuiAlgumDispositivoAntifurto']:checked").val();

		if (possuiAlgumDispositivoAntifurto === "Sim"){
			fv.enableFieldValidators('quaisDispositivosAntifurto', true).revalidateField('quaisDispositivosAntifurto');
			$(".quaisDispositivosAntifurto").removeClass('hide')
		}else{
			$(".quaisDispositivosAntifurto").addClass('hide')
			fv.enableFieldValidators('quaisDispositivosAntifurto', false).revalidateField('quaisDispositivosAntifurto');
		}
	}).on('change', '[name="kitGas"], [name="incluirKitGas"]', function(e){
		var fv = $(".cotacao.auto").data("formValidation");
		var kitGas = $("input[name='kitGas']:checked").val();
		var incluirKitGas = $("input[name='incluirKitGas']:checked").val();
		var incluirKitGasValor = $("input[name='incluirKitGasValor']").val();

		if (kitGas === "Sim"){
			fv.enableFieldValidators('incluirKitGas', true).revalidateField('incluirKitGas');
			$(".incluirKitGas").removeClass('hide')
			if (incluirKitGas === "Sim"){
				fv.enableFieldValidators('incluirKitGasValor', true).revalidateField('incluirKitGasValor');
				$(".incluirKitGasValor").removeClass('hide')
			}else{
				fv.enableFieldValidators('incluirKitGasValor', false).revalidateField('incluirKitGasValor');
				$(".incluirKitGasValor").addClass('hide')
			}
		}else{
			$(".incluirKitGas").addClass('hide')
			$(".incluirKitGasValor").addClass('hide')
			fv.enableFieldValidators('incluirKitGasValor', false).revalidateField('incluirKitGasValor');
			fv.enableFieldValidators('incluirKitGasValor', false).revalidateField('incluirKitGasValor');
		}
	}).on('change', '[name="possuiSeguro"]', function(e){
		var fv = $(".cotacao.auto").data("formValidation");
		var validator = $("input[name='possuiSeguro']:checked").val() === "Sim" ? true : false;

		$(".mesmoModeloDaApoliceAnterior, .seguradoraAnterior, .vencimentoDaApolice, .classeBonusApoliceAnterior, .quantosSinistrosOcorreram, .bancoQueTemConta").toggleClass('hide', !validator)
		fv.enableFieldValidators('mesmoModeloDaApoliceAnterior', validator).revalidateField('mesmoModeloDaApoliceAnterior');
		fv.enableFieldValidators('seguradoraAnterior', validator).revalidateField('seguradoraAnterior');
		fv.enableFieldValidators('vencimentoDaApolice', validator).revalidateField('vencimentoDaApolice');
		fv.enableFieldValidators('classeBonusApoliceAnterior', validator).revalidateField('classeBonusApoliceAnterior');
		fv.enableFieldValidators('quantosSinistrosOcorreram', validator).revalidateField('quantosSinistrosOcorreram');
		fv.enableFieldValidators('bancoQueTemConta', validator).revalidateField('bancoQueTemConta');
	}).on('change', '[name="taxi"]', function (e) {
		var fv = $(".cotacao.auto").data("formValidation");
		var taxi = $("input[name='taxi']:checked").val();

		if (taxi == "Sim") {
			fv.enableFieldValidators('taxiIsencaoImposto', true).revalidateField('taxiIsencaoImposto');
			$(".taxiIsencaoImposto").removeClass('hide')
		} else {
			fv.enableFieldValidators('taxiIsencaoImposto', false).revalidateField('taxiIsencaoImposto');
			$(".taxiIsencaoImposto").addClass('hide')
		}
	}).on('change', '[name="qtdResidentesMenoresde25Anos"]', function (e) {
		$(".form-group.agePeople:not(#templateAgePeople)").remove();

		var qtd = $("input[name='qtdResidentesMenoresde25Anos']:checked").val();
		var $template = $("#templateAgePeople");
		if (!$(".form-subgroup").hasClass('hide')){
			$(".form-subgroup").addClass('hide')
		}
		if (qtd > 0) {
			for (var i = 0; i < qtd; i++) {
				var $clone = $template.clone().removeClass('hide').removeAttr('id').attr("data-agePeople-index", i).insertBefore($template);
				$clone.find('[name="idade"]').attr('name', 'condutor['+i+'].idade');
				$clone.find("legend").html((i+1)+"ª Pessoa");
				$('.cotacao.auto').formValidation('addField', 'condutor['+i+'].idade', agePeopleDriverValidatiors)
			}
			$(".form-subgroup").removeClass('hide');
		}
	}).on('keyup', '[name="dataNascimento"]', function (e) {
		var data = e.target.value
		if (data.length > 0){
			var yearBorn = data.split("/")[2];
			var yearNow = new Date().getFullYear();
			if (yearBorn.search("a") == -1){
				$("select.anoPrimeiraHabilitacaoSegurado").html("");
				for (var i = parseInt(yearBorn)+18; i <= yearNow; i++) {
					$("select.anoPrimeiraHabilitacaoSegurado").append('<option value="'+i+'">'+i+'</option>')
				}
				$(".form-group.anoPrimeiraHabilitacaoSegurado").removeClass('hide');
			}else if (!$(".form-group.anoPrimeiraHabilitacaoSegurado").hasClass('hide')){
				$(".form-group.anoPrimeiraHabilitacaoSegurado").addClass('hide');
				$("select.anoPrimeiraHabilitacaoSegurado").html("");
			}
		}
	}).on('keyup', '[name="dataNascimentoCondutor"]', function (e) {
		var data = e.target.value
		var yearBorn = data.split("/")[2];
		var yearNow = new Date().getFullYear();
		if (yearBorn.search("a") == -1){
			$("select.anoPrimeiraHabilitacao").html("");
			for (var i = parseInt(yearBorn)+18; i <= yearNow; i++) {
				$("select.anoPrimeiraHabilitacao").append('<option value="'+i+'">'+i+'</option>')
			}
			$(".form-group.anoPrimeiraHabilitacao").removeClass('hide');
		}else if (!$(".form-group.anoPrimeiraHabilitacao").hasClass('hide')){
			$(".form-group.anoPrimeiraHabilitacao").addClass('hide');
			$("select.anoPrimeiraHabilitacao").html("");
		}
	}).on('change', '[name="usoParaFaculdade"]', function (e) {
		var fv = $(".cotacao.auto").data("formValidation");
		var garagem = $("input[name='usoParaFaculdade']:checked").val();

		if (garagem == "Sim"){
			fv.enableFieldValidators('possuiGaragemFaculdade', true).revalidateField('possuiGaragemFaculdade')
			$(".possuiGaragemFaculdade").removeClass('hide')
		}else{
			fv.enableFieldValidators('possuiGaragemFaculdade', false).revalidateField('possuiGaragemFaculdade')
			$(".possuiGaragemFaculdade").addClass('hide')
		}
	}).on('change', '[name="usoParaTrabalho"]', function (e) {
		var fv = $(".cotacao.auto").data("formValidation");
		var trabalho = $("input[name='usoParaTrabalho']:checked").val();
		var validator = (trabalho === "Sim") ? true : false
		fv.enableFieldValidators('distanciaResidenciaTrabalho', validator).revalidateField('distanciaResidenciaTrabalho')
		$(".distanciaResidenciaTrabalho").toggleClass('hide', !validator)

		fv.enableFieldValidators('possuiGaragemTrabalho', validator).revalidateField('possuiGaragemTrabalho')
		$(".possuiGaragemTrabalho").toggleClass('hide', !validator)
	});
});