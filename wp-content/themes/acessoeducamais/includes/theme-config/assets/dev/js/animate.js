function animatedHome(){
	$("#navbar").viewportChecker({
		classToAdd: "animated fadeInUp",
		repeat: false
	});

	$("#seguro-auto, #porque-acesso, #seguros, #depoimentos, #blog, #faq, #contato, #footer, .carousel, .page-insurance, .page-insurance-quickly, .wrapper, .container-fluid.wrapper").viewportChecker({
		classToAdd: "animated fadeInUp",
		repeat: false
	})
}

$(function () {
	animatedHome()
});