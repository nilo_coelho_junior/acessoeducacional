function baseUrl () {
  if (location.host == 'localhost' || location.host == '127.0.0.1') {
    return '/acessoeducamais/'
  } else {
    return '/'
  }
}

function scrollToTop () {
  $('body').stop().animate({scrollTop: 0}, '500', 'swing')
}

$(function () {

})
