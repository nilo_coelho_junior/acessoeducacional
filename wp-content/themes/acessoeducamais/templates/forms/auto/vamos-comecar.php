<h2 class="text-center"><strong>Legal vai ser rapidinho, vamos lá ...</strong></h2>
<h4 class="text-center"><i>Insira as informações do segurado</i></h4>
<input type="hidden" name="section" value="Insira as informações do segurado">
<br>
<div class="form-group">
  <label for="nomeCompleto" class="col-sm-6 control-label">Qual seu nome completo?</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="nomeCompleto" name="nomeCompleto" placeholder="Nome completo sem abreviações" required>
  </div>
</div>

<div class="form-group">
  <label for="cpfSegurado" class="col-sm-6 control-label">CPF do segurado</label>
  <div class="col-sm-4">
		<input type="text" data-mask="cpf" class="form-control" name="cpfSegurado" id="cpfSegurado" placeholder="" required>
  </div>
</div>

<div class="form-group">
  <label for="celularContato" class="col-sm-6 control-label">Telefone para Contato </label>
  <div class="col-sm-4">
		<input type="text" data-mask="phone" class="form-control" name="celularContato" id="celularContato" placeholder="" required>
  </div>
</div>

<div class="form-group">
  <label for="emailContato" class="col-sm-6 control-label">E-mail</label>
  <div class="col-sm-4">
		<input type="email" class="form-control" name="emailContato" id="emailContato" placeholder="" required>
  </div>
</div>

<div class="form-group">
  <label for="sexo" class="col-sm-6 control-label">Sexo</label>
  <div class="col-sm-4">
  	<div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default ">
		    <input type="radio" name="sexo" id="sexo" value="Masculino" required> Masculino
		  </label>
		  <label class="btn btn-default">
		    <input type="radio" name="sexo" id="sexo" value="Feminino"> Feminino
		  </label>		  
		</div>
  </div>
</div>

<div class="form-group dataNascimento">
  <label for="dataNascimento" class="col-sm-6 control-label">Data de Nascimento</label>
  <div class="col-sm-4">
		<input type="text" data-mask='date' class="form-control" name="dataNascimento" id="dataNascimentoContato" placeholder="dd/mm/aaaa" required>
  </div>
</div>

<div class="form-group">
  <label for="estadoCivil" class="col-sm-6 control-label">Estado Civil</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="estadoCivil" id="estadoCivil" value="Casado(a) ou reside há pelo menos 2 anos com companheiro(a)" required> Casado(a) ou reside há pelo menos 2 anos com companheiro(a)
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="estadoCivil" id="estadoCivil" value="Separado(a)/Divorciado(a)"> Separado(a)/Divorciado(a)
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="estadoCivil" id="estadoCivil" value="Solteiro(a)"> Solteiro(a)
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="estadoCivil" id="estadoCivil" value="Viúvo(a)"> Viúvo(a)
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="ehPrincipalCondutor" class="col-sm-6 control-label">
  	Você é o principal condutor do veículo?
  	<small>O principal condutor dirige por, pelo menos, 85% do tempo de uso do veículo</small>
  </label>
  <div class="col-sm-4">
  	<div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default ">
		    <input type="radio" name="ehPrincipalCondutor" id="ehPrincipalCondutor" value="Sim" required> Sim
		  </label>
		  <label class="btn btn-default">
		    <input type="radio" name="ehPrincipalCondutor" id="ehPrincipalCondutor" value="Não"> Não
		  </label>		  
		</div>
  </div>
</div>