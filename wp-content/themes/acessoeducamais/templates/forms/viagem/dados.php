<h2 class="text-center"><strong>Legal vai ser rapidinho, vamos lá ...</strong></h2>
<br>

<div class="form-group">
  <label for="saida" class="col-sm-6 control-label">Saída</label>
  <div class="col-sm-3">
    <select class="form-control" name="saida" required>
    	<option selected disabled>Selecione um Estado</option>
			<option value="AC">AC</option>
			<option value="AL">AL</option>
			<option value="AP">AP</option>
			<option value="AM">AM</option>
			<option value="BA">BA</option>
			<option value="DF">DF</option>
			<option value="CE">CE</option>
			<option value="ES">ES</option>
			<option value="GO">GO</option>
			<option value="MA">MA</option>
			<option value="MT">MT</option>
			<option value="MS">MS</option>
			<option value="MG">MG</option>
			<option value="PA">PA</option>
			<option value="PB">PB</option>
			<option value="PR">PR</option>
			<option value="PE">PE</option>
			<option value="PI">PI</option>
			<option value="RJ">RJ</option>
			<option value="RN">RN</option>
			<option value="RS">RS</option>
			<option value="RO">RO</option>
			<option value="RR">RR</option>
			<option value="SC">SC</option>
			<option value="SP">SP</option>
			<option value="SE">SE</option>
			<option value="TO">TO</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label for="primeiroPaisDestino" class="col-sm-6 control-label">Primeiro país de destino</label>
	  <div class="col-sm-3">
	    <select class="form-control" name="primeiroPaisDestino" required>
	    	<option selected disabled>Selecione um País</option>
				<option value="Afeganistão">Afeganistão</option>
				<option value="África do Sul">África do Sul</option>
				<option value="Albânia">Albânia</option>
				<option value="Alderney">Alderney</option>
				<option value="Alemanha">Alemanha</option>
				<option value="Andorra">Andorra</option>
				<option value="Angola">Angola</option>
				<option value="Anguilla">Anguilla</option>
				<option value="Antartida">Antartida</option>
				<option value="Antígua e Barbuda">Antígua e Barbuda</option>
				<option value="Antilhas Holandesas">Antilhas Holandesas</option>
				<option value="Arábia Saudita">Arábia Saudita</option>
				<option value="Argélia">Argélia</option>
				<option value="Argentina">Argentina</option>
				<option value="Armênia">Armênia</option>
				<option value="Aruba">Aruba</option>
				<option value="Austrália">Austrália</option>
				<option value="Áustria">Áustria</option>
				<option value="Azerbaijão">Azerbaijão</option>
				<option value="Bahamas">Bahamas</option>
				<option value="Bahrein">Bahrein</option>
				<option value="Bangladesh">Bangladesh</option>
				<option value="Barbados">Barbados</option>
				<option value="Belarus">Belarus</option>
				<option value="Bélgica">Bélgica</option>
				<option value="Belize">Belize</option>
				<option value="Benin">Benin</option>
				<option value="Bermudas">Bermudas</option>
				<option value="Bolívia">Bolívia</option>
				<option value="Bósnia-Herzegóvina">Bósnia-Herzegóvina</option>
				<option value="Botsuana">Botsuana</option>
				<option value="Brasil">Brasil</option>
				<option value="Brunei">Brunei</option>
				<option value="Bulgária">Bulgária</option>
				<option value="Burkina Fasso">Burkina Fasso</option>
				<option value="Burundi">Burundi</option>
				<option value="Butão">Butão</option>
				<option value="Cabo Verde">Cabo Verde</option>
				<option value="Camarões">Camarões</option>
				<option value="Camboja">Camboja</option>
				<option value="Canadá">Canadá</option>
				<option value="Cazaquistão">Cazaquistão</option>
				<option value="Chade">Chade</option>
				<option value="Chile">Chile</option>
				<option value="China">China</option>
				<option value="Chipre">Chipre</option>
				<option value="Cingapura">Cingapura</option>
				<option value="Colômbia">Colômbia</option>
				<option value="Comoros">Comoros</option>
				<option value="Congo">Congo</option>
				<option value="Coréia do Norte">Coréia do Norte</option>
				<option value="Coréia do Sul">Coréia do Sul</option>
				<option value="Costa do Marfim">Costa do Marfim</option>
				<option value="Costa Rica">Costa Rica</option>
				<option value="Croácia">Croácia</option>
				<option value="Cuba">Cuba</option>
				<option value="Dinamarca">Dinamarca</option>
				<option value="Djibuti">Djibuti</option>
				<option value="Dominica">Dominica</option>
				<option value="Egito">Egito</option>
				<option value="El Salvador">El Salvador</option>
				<option value="Emirados Árabes Unidos">Emirados Árabes Unidos</option>
				<option value="Equador">Equador</option>
				<option value="Eritreia">Eritreia</option>
				<option value="Escócia">Escócia</option>
				<option value="Eslováquia">Eslováquia</option>
				<option value="Eslovênia">Eslovênia</option>
				<option value="Espanha">Espanha</option>
				<option value="Estados Unidos">Estados Unidos</option>
				<option value="Estônia">Estônia</option>
				<option value="Etiópia">Etiópia</option>
				<option value="Fiji">Fiji</option>
				<option value="Filipinas">Filipinas</option>
				<option value="Finlândia">Finlândia</option>
				<option value="França">França</option>
				<option value="Gabão">Gabão</option>
				<option value="Gâmbia">Gâmbia</option>
				<option value="Gana">Gana</option>
				<option value="Geórgia">Geórgia</option>
				<option value="Georgia do Sul e Ilhas Sandui">Georgia do Sul e Ilhas Sandui</option>
				<option value="Gibraltar">Gibraltar</option>
				<option value="Granada">Granada</option>
				<option value="Grécia">Grécia</option>
				<option value="Groelândia">Groelândia</option>
				<option value="Guadalupe (FR)">Guadalupe (FR)</option>
				<option value="Guam">Guam</option>
				<option value="Guatemala">Guatemala</option>
				<option value="Guiana">Guiana</option>
				<option value="Guiana Francesa">Guiana Francesa</option>
				<option value="Guiné">Guiné</option>
				<option value="Guiné Equatorial">Guiné Equatorial</option>
				<option value="Guiné-Bissau">Guiné-Bissau</option>
				<option value="Haiti">Haiti</option>
				<option value="Holanda">Holanda</option>
				<option value="Honduras">Honduras</option>
				<option value="Hong Kong">Hong Kong</option>
				<option value="Hungria">Hungria</option>
				<option value="Iêmen">Iêmen</option>
				<option value="Ilha Bouvet">Ilha Bouvet</option>
				<option value="Ilha de Reunião">Ilha de Reunião</option>
				<option value="Ilha do Homem">Ilha do Homem</option>
				<option value="Ilha Guernsey">Ilha Guernsey</option>
				<option value="Ilha Heard e Ilhas Mcdonald">Ilha Heard e Ilhas Mcdonald</option>
				<option value="Ilha Jersey">Ilha Jersey</option>
				<option value="Ilha Natal">Ilha Natal</option>
				<option value="Ilha Norfolk">Ilha Norfolk</option>
				<option value="Ilha Pitcairn">Ilha Pitcairn</option>
				<option value="Ilha Reuniao">Ilha Reuniao</option>
				<option value="Ilhas Cayman">Ilhas Cayman</option>
				<option value="Ilhas Cocos">Ilhas Cocos</option>
				<option value="Ilhas Comores">Ilhas Comores</option>
				<option value="Ilhas Cook">Ilhas Cook</option>
				<option value="Ilhas Faeroes">Ilhas Faeroes</option>
				<option value="Ilhas Falkland (Malvinas)">Ilhas Falkland (Malvinas)</option>
				<option value="Ilhas Faroe">Ilhas Faroe</option>
				<option value="Ilhas Marianas do Norte">Ilhas Marianas do Norte</option>
				<option value="Ilhas Marshall">Ilhas Marshall</option>
				<option value="Ilhas Natal">Ilhas Natal</option>
				<option value="Ilhas Salomão">Ilhas Salomão</option>
				<option value="Ilhas Seychelles">Ilhas Seychelles</option>
				<option value="Ilhas Svalbard">Ilhas Svalbard</option>
				<option value="Ilhas Tokelau">Ilhas Tokelau</option>
				<option value="Ilhas Turks e Caicos">Ilhas Turks e Caicos</option>
				<option value="Ilhas Virgens (UK)">Ilhas Virgens (UK)</option>
				<option value="Ilhas Virgens (US)">Ilhas Virgens (US)</option>
				<option value="Ilhas Wallis e Futuna">Ilhas Wallis e Futuna</option>
				<option value="Índia">Índia</option>
				<option value="Indonésia">Indonésia</option>
				<option value="Inglaterra">Inglaterra</option>
				<option value="Irã">Irã</option>
				<option value="Iraque">Iraque</option>
				<option value="Irlanda">Irlanda</option>
				<option value="Irlanda do Norte">Irlanda do Norte</option>
				<option value="Islândia">Islândia</option>
				<option value="Israel">Israel</option>
				<option value="Itália">Itália</option>
				<option value="Iugoslavia">Iugoslavia</option>
				<option value="Jamaica">Jamaica</option>
				<option value="Japão">Japão</option>
				<option value="Jordânia">Jordânia</option>
				<option value="Kiribati">Kiribati</option>
				<option value="Kuawait">Kuawait</option>
				<option value="Laos">Laos</option>
				<option value="Lesoto">Lesoto</option>
				<option value="Letônia">Letônia</option>
				<option value="Líbano">Líbano</option>
				<option value="Libéria">Libéria</option>
				<option value="Líbia">Líbia</option>
				<option value="Liechtenstein">Liechtenstein</option>
				<option value="Lituânia">Lituânia</option>
				<option value="Luxemburgo">Luxemburgo</option>
				<option value="Macau">Macau</option>
				<option value="Macedônia">Macedônia</option>
				<option value="Madagascar">Madagascar</option>
				<option value="Malásia">Malásia</option>
				<option value="Malawi">Malawi</option>
				<option value="Maldivas">Maldivas</option>
				<option value="Mali">Mali</option>
				<option value="Malta">Malta</option>
				<option value="Marrocos">Marrocos</option>
				<option value="Martinica">Martinica</option>
				<option value="Maurício">Maurício</option>
				<option value="Mauritânia">Mauritânia</option>
				<option value="Mayotte">Mayotte</option>
				<option value="México">México</option>
				<option value="Micronésia">Micronésia</option>
				<option value="Moçambique">Moçambique</option>
				<option value="Moldávia">Moldávia</option>
				<option value="Mônaco">Mônaco</option>
				<option value="Mongólia">Mongólia</option>
				<option value="Montserrat">Montserrat</option>
				<option value="Myanmar">Myanmar</option>
				<option value="Namíbia">Namíbia</option>
				<option value="Nauru">Nauru</option>
				<option value="Nepal">Nepal</option>
				<option value="Nicarágua">Nicarágua</option>
				<option value="Níger">Níger</option>
				<option value="Nigéria">Nigéria</option>
				<option value="Niue">Niue</option>
				<option value="Noruega">Noruega</option>
				<option value="Nova Caledônia">Nova Caledônia</option>
				<option value="Nova Zelândia">Nova Zelândia</option>
				<option value="Omã">Omã</option>
				<option value="Outras Ilhas Menores dos EUA">Outras Ilhas Menores dos EUA</option>
				<option value="País de Gales">País de Gales</option>
				<option value="Palau">Palau</option>
				<option value="Panamá">Panamá</option>
				<option value="Papua-Nova Guiné">Papua-Nova Guiné</option>
				<option value="Paquistão">Paquistão</option>
				<option value="Paraguai">Paraguai</option>
				<option value="Peru">Peru</option>
				<option value="Polinésia Francesa">Polinésia Francesa</option>
				<option value="Polônia">Polônia</option>
				<option value="Porto Rico">Porto Rico</option>
				<option value="Portugal">Portugal</option>
				<option value="Qatar">Qatar</option>
				<option value="Quênia">Quênia</option>
				<option value="Quirguistão">Quirguistão</option>
				<option value="República Centro-Africana">República Centro-Africana</option>
				<option value="Republica Democratica do Congo">Republica Democratica do Congo</option>
				<option value="República Dominicana">República Dominicana</option>
				<option value="República Tcheca">República Tcheca</option>
				<option value="Reunion">Reunion</option>
				<option value="Romênia">Romênia</option>
				<option value="Ruanda">Ruanda</option>
				<option value="Russia">Russia</option>
				<option value="Saara Ocidental">Saara Ocidental</option>
				<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
				<option value="Saint Pierre ET Miquelo">Saint Pierre ET Miquelo</option>
				<option value="Samoa Ocidental">Samoa Ocidental</option>
				<option value="Samoas Americanas">Samoas Americanas</option>
				<option value="San Marino">San Marino</option>
				<option value="Santa Helena">Santa Helena</option>
				<option value="Santa Lúcia">Santa Lúcia</option>
				<option value="São Cristovão e Nevis">São Cristovão e Nevis</option>
				<option value="São Tomé e Príncipe">São Tomé e Príncipe</option>
				<option value="São Vicente e Granadinas">São Vicente e Granadinas</option>
				<option value="Senegal">Senegal</option>
				<option value="Serra Leoa">Serra Leoa</option>
				<option value="Servia e Montenegro">Servia e Montenegro</option>
				<option value="Singapura">Singapura</option>
				<option value="Síria">Síria</option>
				<option value="Somália">Somália</option>
				<option value="Sri Lanka">Sri Lanka</option>
				<option value="Suazilândia">Suazilândia</option>
				<option value="Sudão">Sudão</option>
				<option value="Suécia">Suécia</option>
				<option value="Suíça">Suíça</option>
				<option value="Suriname">Suriname</option>
				<option value="Svalbard e Jan Mayen">Svalbard e Jan Mayen</option>
				<option value="Tadjiquistão">Tadjiquistão</option>
				<option value="Tailândia">Tailândia</option>
				<option value="Taiwan">Taiwan</option>
				<option value="Tanzânia">Tanzânia</option>
				<option value="Terras Austrais Francesas">Terras Austrais Francesas</option>
				<option value="Territ Britan do Oceano Indico">Territ Britan do Oceano Indico</option>
				<option value="Timor-Leste">Timor-Leste</option>
				<option value="Togo">Togo</option>
				<option value="Tonga">Tonga</option>
				<option value="Trinidad e Tobago">Trinidad e Tobago</option>
				<option value="Tunísia">Tunísia</option>
				<option value="Turcomenistão">Turcomenistão</option>
				<option value="Turquia">Turquia</option>
				<option value="Tuvalu">Tuvalu</option>
				<option value="Ucrânia">Ucrânia</option>
				<option value="Uganda">Uganda</option>
				<option value="Uruguai">Uruguai</option>
				<option value="Uzbequistão">Uzbequistão</option>
				<option value="Vanuatu">Vanuatu</option>
				<option value="Vaticano">Vaticano</option>
				<option value="Venezuela">Venezuela</option>
				<option value="Vietnã">Vietnã</option>
				<option value="Zaire">Zaire</option>
				<option value="Zâmbia">Zâmbia</option>
				<option value="Zimbábue">Zimbábue</option>
	    </select>
	  </div>
</div>

<div class="form-group">
  <label for="dataIda" class="col-sm-6 control-label">Data de Ida</label>
  <div class="col-sm-2">
    <input type="text" data-mask='date' class="form-control" id="dataIda" name="dataIda" required>
  </div>
</div>

<div class="form-group">
  <label for="dataVolta" class="col-sm-6 control-label">Data de Volta</label>
  <div class="col-sm-2">
    <input type="text" data-mask='date' class="form-control" id="dataVolta" name="dataVolta" required>
  </div>
</div>

<div class="form-group">
  <label for="numeroPassageiros0-71Anos" class="col-sm-6 control-label">Número de Passageiros</label>
  <div class="col-sm-2">
    <select class="form-control" name="numeroPassageiros0-71Anos" aria-describedby="help071" required>
			<option value="01">01</option>
			<option value="02">02</option>
			<option value="03">03</option>
			<option value="04">04</option>
			<option value="05">05</option>
			<option value="06">06</option>
			<option value="07">07</option>
			<option value="08">08</option>
			<option value="09">09</option>
    </select>
    <span id="help071" class="help-block">0 a 71 anos</span>
  </div>
  <div class="col-sm-2">
    <select class="form-control" name="numeroPassageiros71-85Anos" aria-describedby="help7185" required>
    	<option value="00">00</option>
			<option value="01">01</option>
			<option value="02">02</option>
			<option value="03">03</option>
			<option value="04">04</option>
			<option value="05">05</option>
			<option value="06">06</option>
			<option value="07">07</option>
			<option value="08">08</option>
			<option value="09">09</option>
    </select>
    <span id="help7185" class="help-block">71 a 85 anos</span>
  </div>
</div>

<div class="form-group">
  <label for="comoSeraSuaViagem" class="col-sm-6 control-label">Como será sua viagem?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="comoSeraSuaViagem" id="comoSeraSuaViagem" value="Aérea" required> Aérea
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="comoSeraSuaViagem" id="comoSeraSuaViagem" value="Marítima"> Marítima
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="viagemParaPraticaDeEsportesDeAventura" class="col-sm-6 control-label">Viagem para prática de esportes e aventura?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="viagemParaPraticaDeEsportesDeAventura" id="viagemParaPraticaDeEsportesDeAventura" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="viagemParaPraticaDeEsportesDeAventura" id="viagemParaPraticaDeEsportesDeAventura" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="motivoDaViagem" class="col-sm-6 control-label">Motivo da viagem</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="motivoDaViagem" id="motivoDaViagem" value="Lazer" required> Lazer
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="motivoDaViagem" id="motivoDaViagem" value="Negócios"> Negócios
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="motivoDaViagem" id="motivoDaViagem" value="Estudo"> Estudo
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="motivoDaViagem" id="motivoDaViagem" value="Outros"> Outros
			</label>
		</div>
  </div>
</div>