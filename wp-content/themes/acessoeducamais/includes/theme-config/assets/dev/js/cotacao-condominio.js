function wizardButtonsNameCondominio(index){
	switch (index) {
		case 0:
			$('.cotacao .previous').addClass('disabled')
			break;
		case 1:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').text("Entendi, vamos em frente")
			break;
		case 2:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').removeClass('disabled').text("Ufa! Terminamos")
			break;
		default:
			break;
	}
}

$(function() {

	$(".cotacao.condominio").formValidation({
		framework: "bootstrap",
		locale: "pt_BR",
		fields: {
			cep: {
				validators: {
					notEmpty: {
						message: "Insira o cep do imóvel"
					},
					zipCode:{
						country: "BR"
					}
				}
			},
			desejaSeguroParaEquipamentos: {
				enabled: false
			}
		}
	}).on('change', '[name="atividadeComercialNoImovel"]', function (e) {
		var fv = $(".cotacao.condominio").data("formValidation");
		var taxi = $("input[name='atividadeComercialNoImovel']:checked").val();

		if (taxi == "Sim") {
			fv.enableFieldValidators('desejaSeguroParaEquipamentos', true).revalidateField('desejaSeguroParaEquipamentos');
			$(".desejaSeguroParaEquipamentos").removeClass('hide')
		} else {
			fv.enableFieldValidators('desejaSeguroParaEquipamentos', false).revalidateField('desejaSeguroParaEquipamentos');
			$(".desejaSeguroParaEquipamentos").addClass('hide')
		}
	})
});