<?php 

global $parent;

?>

<!-- Nav tabs -->
<ul class="nav nav-tabs steps hide-for-small-only" role="tablist">
  <li role="presentation" class="active">
  	<a href="#first" aria-controls="first" role="tab" data-toggle="tab">
      	<span class="visible-sm visible-xs">1</span>
      	<span class="hidden-sm hidden-xs">Dados da Casa</span>
  	</a>
  </li>
  <li role="presentation" class="">
  	<a href="#second" aria-controls="second" role="tab" data-toggle="tab">
      	<span class="visible-sm visible-xs">2</span>
  	    <span class="hidden-sm hidden-xs">Características</span>
      </a>
    </li>
  <li role="presentation" class="">
  	<a href="#third" aria-controls="third" role="tab" data-toggle="tab">
  		<span class="visible-sm visible-xs">3</span>
  	    <span class="hidden-sm hidden-xs">Seus Dados</span>
  	</a>
  </li>
</ul>
<!-- Tab panes -->
<?php get_template_part('templates/loading'); ?>
<div class="tab-content container">
  <div role="tabpanel" class="tab-pane active" id="first">
  	<?php get_template_part('templates/forms/'.$parent->post_name.'/dados-casa'); ?>
  </div>
  <div role="tabpanel" class="tab-pane" id="second">
  	<?php get_template_part('templates/forms/'.$parent->post_name.'/caracteristicas'); ?>
  </div>
  <div role="tabpanel" class="tab-pane" id="third">
  	<?php get_template_part('templates/forms/'.$parent->post_name.'/contato'); ?>
  </div>
  <div class="container">
		<div class="col-lg-6 text-right">
			<button class="btn btn-default btn-lg previous">Voltar</button>
		</div>
		<div class="col-lg-6">
			<button class="btn btn-primary btn-lg next">Ótimo! Próximo passo</button>
		</div>
	</div>
</div>