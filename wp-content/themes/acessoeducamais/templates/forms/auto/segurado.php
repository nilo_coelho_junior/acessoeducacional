<h2 class="text-center"><strong>Dados do Segurado</strong></h2>
<input type="hidden" name="section" value="Dados do Segurado">
<br>
<div class="form-group">
  <label for="profissaoSegurado" class="col-sm-6 control-label">Qual é a profissão do segurado? </label>
  <div class="col-sm-4">
		<input type="text" class="form-control" name="profissaoSegurado" id="profissaoSegurado" placeholder="Digite a profissão" required>
  </div>
</div>
<div class="form-group">
  <label for="anoPrimeiraHabilitacaoSegurado" class="col-sm-6 control-label">Ano da primeira habilitação?</label>
  <div class="col-sm-4">
    <select class="form-control anoPrimeiraHabilitacaoSegurado" name="anoPrimeiraHabilitacaoSegurado" required>
    	<option>18</option>
    </select>
  </div>
</div>
<div class="form-group">
  <label for="moradiaSegurado" class="col-sm-6 control-label">O segurado mora em:</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="moradiaSegurado" id="moradiaSegurado" value="Apartamento ou flat" required> Apartamento ou flat
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="moradiaSegurado" id="moradiaSegurado" value="Casa em condominio fechado"> Casa em condominio fechado
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="moradiaSegurado" id="moradiaSegurado" value="Casa ou sobrado"> Casa ou sobrado
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="moradiaSegurado" id="moradiaSegurado" value="Chácara, fazenda ou sítio"> Chácara, fazenda ou sítio
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="moradiaSegurado" id="moradiaSegurado" value="Outros"> Outros
			</label>
		</div>
  </div>
</div>
<div class="form-group">
  <label for="qtdResidentesMenoresde25Anos" class="col-sm-6 control-label">Quantidade de residentes menores de 25 anos?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="0" required> Não
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="1"> 1
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="2"> 2
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="3"> 3
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="4"> 4
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="5"> 5
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="6"> 6
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="7"> 7
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="8"> 8
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="qtdResidentesMenoresde25Anos" id="qtdResidentesMenoresde25Anos" value="9"> 9
			</label>
		</div>
  </div>
</div>
<div class="form-group">
	<div class="form-subgroup col-lg-6 col-md-push-3 hide">
		<div class="arrow"></div>
		<div class="form-group hide agePeople" id="templateAgePeople">
			<fieldset>
				<legend class="col-lg-12 text-center">1ª Pessoa</legend>
				<div class="form-group">
				  <label class="col-sm-4 control-label">Nome Completo</label>
				  <div class="col-sm-8">
						<input type="text" class="form-control" name="nome" required>
				  </div>
			  </div>
				<div class="form-group">
				  <label for="sexo" class="col-sm-4 control-label">Sexo</label>
				  <div class="col-sm-8">
				    <div class="btn-group" data-toggle="buttons">
						  <label class="btn btn-default">
							  <input type="radio" name="sexo" id="sexo" value="Masculino" required> Masculino</label>
							<label class="btn btn-default">
							  <input type="radio" name="sexo" id="sexo" value="Feminino"> Feminino
							</label>
						</div>
				  </div>
				</div>
			  <div class="form-group dtNascimento">
				  <label for="dtNascimento" class="col-sm-4 control-label">Data de Nascimento</label>
				  <div class="col-sm-8">
						<input type="text" data-mask='date' class="form-control" name="dataNascimento" id="dtNascimento" placeholder="dd/mm/aaaa" required>
				  </div>
				</div>
				<div class="form-group">
				  <label class="col-sm-4 control-label">CNH</label>
				  <div class="col-sm-8">
						<input type="text" class="form-control" name="cnh" >
				  </div>
			  </div>
		  </fieldset>
		</div>
	</div>
</div>