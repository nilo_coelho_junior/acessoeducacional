<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="form-inline row" action="<?php echo esc_url( home_url( '/' ) ); ?>">
  <div class="form-group col-lg-12" style="padding-right:-15px;">
    <div class="input-group col-lg-12">
      <input type="search" id="<?php echo $unique_id; ?>" class="form-control search-field" placeholder="Pesquisar ..." value="<?php echo get_search_query(); ?>" name="s" style="width:100%;"/>
      <div class="input-group-btn">
        <button type="submit" class="btn btn-primary search-submit"><span class="icon-search"></span></button>
      </div>
    </div>
  </div>
</form>
