<?php
  get_header();
  $nav_ensino_selected = 'curso';
  $category = get_categories(array(
                'taxonomy' => 'tipo',
                'object_ids' => get_the_ID(),
                'hide_empty' => true,
                'orderby' => 'name'
              ));
  $ensino_id = get_post_meta(get_the_ID(), '_wpcf_belongs_ensino_id', true);
  $ensino = get_post($ensino_id);
  $metodologia = get_curso_field_metodologia(get_the_ID(), $category[0]->term_id);
?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title page-title-small"><?php  echo the_title();  ?></h2>

    <div class="row">
      <div class="col-sm-12">
        <span class="label label-medium label-info">Tipo: <?php echo $ensino->post_title; ?></span>
        <span class="label label-medium label-info">Área: <?php echo $category[0]->name; ?></span>
        <span class="label label-medium label-info">Duração: <?php echo get_curso_field('duracao', get_the_ID(), $category[0]->term_id); ?></span>
        <span class="label label-medium label-info">Carga Horária: <?php echo get_curso_field('carga-horaria', get_the_ID(), $category[0]->term_id); ?></span>
      </div>
    </div>
    <br>
    <ul class="nav nav-pills nav-stacked tipodecurso-nav col-sm-3" role="tablist">
      <li role="presentation" class="active">
        <a href="#apresentacao" role="tab" id="apresentacao-tab" data-toggle="tab" aria-controls="apresentacao" aria-expanded="false">Apresentação</a>
      </li>
      <li role="presentation" class="">
        <a href="#objetivo-curso" role="tab" id="objetivo-curso-tab" data-toggle="tab" aria-controls="objetivo-curso" aria-expanded="false">Objetivo do Curso</a>
      </li>
      <li role="presentation" class="">
        <?php if ($metodologia[0] == 'pdf') { ?>
          <a href="<?php echo $metodologia[1] ?>" target='_blank'>
            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
            Metodologia
          </a>
        <?php } else { ?>
          <a href="#metodologia" role="tab" id="metodologia-tab" data-toggle="tab" aria-controls="metodologia" aria-expanded="false">Metodologia</a>
        <?php } ?>
      </li>
      <li role="presentation" class="">
        <a href="#avaliacoes" role="tab" id="avaliacoes-tab" data-toggle="tab" aria-controls="avaliacoes" aria-expanded="false">Avaliações de Aprendizagem</a>
      </li>
      <li role="presentation" class="">
        <a href="#investimento" role="tab" id="investimento-tab" data-toggle="tab" aria-controls="investimento" aria-expanded="false">Investimento</a>
      </li>
      <li role="presentation" class="">
        <a href="#faculdade" role="tab" id="faculdade-tab" data-toggle="tab" aria-controls="faculdade" aria-expanded="false">Tudo sobre a Faculdade</a>
      </li>
      <li role="presentation" class="">
        <a href="#faq" role="tab" id="faq-tab" data-toggle="tab" aria-controls="faq" aria-expanded="false">Perguntas Frequentes</a>
      </li>
    </ul>


    <div class="tab-content tipodecurso-tab-content col-sm-9">
      <div class="tab-pane fade active in" role="tabpanel" id="apresentacao" aria-labelledby="apresentacao-tab">
         <?php echo get_curso_field('apresentacao', get_the_ID(), $category[0]->term_id); ?>
      </div>
      <div role="tabpanel" class="tab-pane" id="objetivo-curso" aria-labelledby="objetivo-curso-tab">
        <?php echo get_curso_field('objetivo-do-curso', get_the_ID(), $category[0]->term_id); ?>
      </div>
      <div role="tabpanel" class="tab-pane" id="metodologia" aria-labelledby="metodologia-tab">
        <?php echo $metodologia[1]; ?>
      </div>
      <div role="tabpanel" class="tab-pane" id="avaliacoes" aria-labelledby="avaliacoes-tab">
        <?php echo get_curso_field('avaliacao-de-aprendizagem', get_the_ID(), $category[0]->term_id); ?>
      </div>
      <div role="tabpanel" class="tab-pane" id="investimento" aria-labelledby="investimento-tab">
        <?php echo get_curso_field('investimento', get_the_ID(), $category[0]->term_id); ?>
      </div>

      <div class="tab-pane fade" role="tabpanel" id="faculdade" aria-labelledby="faculdade-tab">
        <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
      </div>
      <div class="tab-pane fade" role="tabpanel" id="faq" aria-labelledby="faq-tab">
        <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
      </div>
    </div>

  </div>
</div>

<?php get_footer(); ?>
