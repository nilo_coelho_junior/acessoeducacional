function validateTab (index) {
  var fv = $('.cotacao').data('formValidation'), // FormValidation instance
	  // The current tab
	  $tab = $('.cotacao').find('.tab-pane').eq(index)
  // Validate the container
  fv.validateContainer($tab)
  var isValidStep = fv.isValidContainer($tab)
  if (isValidStep === false || isValidStep === null) {
    // Do not jump to the target tab
    return false
  }
  return true
}

function wizardButtonsName (index) {
  if ($('.cotacao.auto').length > 0) {
    wizardButtonsNameAuto(index)
  } else if ($('.cotacao.residencial').length > 0) {
    wizardButtonsNameResidencial(index)
  } else if ($('.cotacao.condominio').length > 0) {
    wizardButtonsNameCondominio(index)
  } else if ($('.cotacao.vida').length > 0) {
    wizardButtonsNameVida(index)
  } else if ($('.cotacao.empresarial').length > 0) {
    wizardButtonsNameEmpresarial(index)
  } else if ($('.cotacao.viagem').length > 0) {
    wizardButtonsNameViagem(index)
  }
}

function scrollToTop () {
  $('body').stop().animate({scrollTop: 0}, '500', 'swing')
}

$(function () {
  $('.cotacao').on('success.form.fv', function (event) {
    console.log('send cotacao')
    var data = $(this).serialize()
    event.preventDefault()
    scrollToTop()
    $('.cotacao .tab-content').hide()
    $('.loading').show()

    $.post(baseUrl() + 'wp-admin/admin-ajax.php', {action: 'send_cotation', data: data}, function (data) {
      data = JSON.parse(data)
      if (data.code == 302) {
        window.location.replace(data.message)
      } else {
        console.log('algo deu errado')
      }
    })
  })

  $('.cotacao').bootstrapWizard({
    tabClass: 'nav nav-tabs steps',
    nextSelector: '.btn-primary.next',
    previousSelector: '.btn-default.previous',
    onInit: function (tab, navigation, index) {
      var numTabs = $('.cotacao').find('.tab-pane').length
      if (index === (numTabs - 1)) {
				 $('.cotacao .next').removeClass('disabled')
      }
      resizeSteps()
    },
    onTabClick: function (tab, navigation, index, next) {
      if (index > next) {
        return true
      } else {
        return validateTab(index)
      }
    },
    onPrevious: function (tab, navigation, index) {
      if ($("input[name='ehPrincipalCondutor']").val() === 'Sim' && index === 2) {
        $('.cotacao').bootstrapWizard('show', 1)
        return false
      }
    },
    onNext: function (tab, navigation, index) {
      var numTabs = $('.steps li').length

      if ($("label.active input[name='ehPrincipalCondutor']").val() === 'Sim' && index === 2) {
        $('.cotacao').bootstrapWizard('show', 3)
        return false
      }

      if (!validateTab(index - 1)) {
        return false
      }

      if (index === (numTabs)) {
        // We are at the last tab
        // Uncomment the following line to submit the form using the defaultSubmit() method
        $('.cotacao .next').removeClass('disabled')
        // $('.cotacao').formValidation('defaultSubmit');
        // For testing purpose
      }
      scrollToTop()
      return true
    },
    onTabShow: function (tab, navigation, index) {
	    // Update the label of Next button when we are at the last tab
	    var numTabs = $('.steps li:visible').length

	    $.each($('.cotacao').find('.nav-tabs li'), function (i, val) {
	    	if (i < index) {
	    		$(val).addClass('ok')
	    	} else {
	    		$(val).removeClass('ok')
	    	}
	    })
	    wizardButtonsName(index)
	  }
  })
})
