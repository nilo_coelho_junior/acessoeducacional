<?php

require_once('theme-assets.php');
require_once('theme-menu.php');
require_once('theme-functions.php');
require_once('theme-widgets.php');
require_once('theme-page.php');

// Wordpress Theme Support

$args = array(
  'flex-width'    => true,
  'flex-height'    => true,
  'upload'    => true,
  'default-image' => get_template_directory_uri() . '/assets/dist/img/logo.png',
);

add_theme_support( 'custom-header', $args );

add_theme_support( 'customize-selective-refresh-widgets' );

add_theme_support( 'post-thumbnails' );

remove_filter ('the_exceprt', 'wpautop');

?>
