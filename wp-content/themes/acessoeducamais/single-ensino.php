<?php
  get_header();
  $nav_ensino_selected = 'cursos';
?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title"><?php the_title(); ?></h2>

    <ul class="nav nav-pills nav-stacked tipodecurso-nav col-sm-3" role="tablist">
      <li role="presentation" class="active">
        <a href="#cursos" id="cursos-tab" role="tab" data-toggle="tab" aria-controls="cursos" aria-expanded="true">Cursos</a>
      </li>
      <li role="presentation" class="">
        <a href="#faculdade" role="tab" id="faculdade-tab" data-toggle="tab" aria-controls="faculdade" aria-expanded="false">Tudo sobre a Faculdade</a>
      </li>
      <li role="presentation" class="">
        <a href="#faq" role="tab" id="faq-tab" data-toggle="tab" aria-controls="faq" aria-expanded="false">Perguntas Frequentes</a>
      </li>
    </ul>


    <div class="tab-content tipodecurso-tab-content col-sm-9">
      <div class="tab-pane fade active in" role="tabpanel" id="cursos" aria-labelledby="cursos-tab">
        <?php get_template_part( './includes/ensino-courses-list'); ?>
      </div>
      <div class="tab-pane fade" role="tabpanel" id="faculdade" aria-labelledby="faculdade-tab">
        <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
      </div>
      <div class="tab-pane fade" role="tabpanel" id="faq" aria-labelledby="faq-tab">
        <h3 class="no-margin"><strong>Perguntas Frequentes</strong></h3>
        <br>
        <?php
          wp_reset_query();
          $faq = new WP_Query(array('post_type' => 'faq', 'meta_query' => array(array('key' => '_wpcf_belongs_ensino_id', 'value' => get_the_ID()))));
          if ( $faq->have_posts() ) : while ( $faq->have_posts() ) : $faq->the_post();
            echo the_content();
          endwhile; else:
            echo '<p>Desculpe. Ainda não temos perguntas cadastradas.</p>';
          endif;
        ?>
      </div>
    </div>

  </div>
</div>

<?php get_footer(); ?>
