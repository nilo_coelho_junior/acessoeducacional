<?php $ensino = get_the_ID(); ?>

<h3 class="no-margin"><strong>Cursos</strong></h3>
<br>
<?php
$curses_id = new WP_Query(array('post_type' => 'curso', 'meta_query' => array(array('key' => '_wpcf_belongs_ensino_id', 'value' => $ensino)), 'fields' => 'ids'));
$curses = types_child_posts('curso');
$categories = get_categories(array(
  'taxonomy' => 'tipo',
  'object_ids' => $curses_id->posts,
  'hide_empty' => true,
  'orderby' => 'name'
));

if (empty($categories) && !empty($curses)){
?>
  <ul class="list-arrow no-pd row">
  <?php
    foreach ($curses as $curse) {
  ?>
      <li class="col-xs-6"><a href="<?php echo $curse->post_permalink; ?>"><?php echo($curse->post_title); ?></a></li>
  <?php
    }
  ?>
  </ul>
<?php
}else{
?>
  <ul class="nav nav-pills nav-amaranth" role="tablist">
    <?php
      $cat_count = 0;
      foreach ($categories as $cat) {
    ?>
      <li role="presentation" class="<?php echo $cat_count == 0 ? 'active' : '';?> text-uppercase">
        <a href="#<?php echo $cat->slug;?>" aria-controls="<?php echo $cat->slug;?>" role="tab" data-toggle="tab">
          <?php echo $cat->cat_name; ?>
        </a>
      </li>
    <?php
        $cat_count++;
      }
    ?>
  </ul>
  <div class="tab-content">
<?php
    $cat_count = 0;
    foreach ($categories as $cat) {
?>
      <div role="tabpanel" class="tab-pane <?php echo $cat_count == 0 ? 'active fade in' : 'fade';?>" id="<?php echo $cat->slug;?>" >
<?php
        $courses_cat = new WP_Query(array(
          'post_type' => 'curso',
          'tax_query' => array(array(
            'taxonomy' => 'tipo',
            'terms' => array($cat->term_id))),
          'meta_query' => array(array(
            'key' => '_wpcf_belongs_ensino_id',
            'value' => $ensino)),
          'orderby' => 'title',
          'order' => 'ASC'
        ));
        $courses_count = 1;
        $per = ceil($courses_cat->post_count/2);
        if ($courses_cat->have_posts()):
?>
          <ul class="list-arrow no-pd col-sm-6">
<?php
            while($courses_cat->have_posts()) : $courses_cat->the_post();
?>
              <li><a href="<?php echo the_permalink(); ?>"><?php echo(the_title()); ?></a></li>

              <?php if ($courses_count == $per) echo '</ul><ul class="list-arrow no-pd col-sm-6">'?>
<?php
              $courses_count++;
            endwhile;
?>
          </ul>
<?php
        endif;
?>
      </div>
<?php
      $cat_count++;
    }
?>
</div>
<?php
  }
?>
