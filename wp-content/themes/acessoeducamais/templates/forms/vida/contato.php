<h2 class="text-center"><strong>Informe seus dados</strong></h2>
<br>

<div class="form-group">
  <label for="nomeDoSegurado" class="col-sm-6 control-label">Nome do Segurado</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="nomeDoSegurado" name="nomeDoSegurado" required>
  </div>
</div>

<div class="form-group dataNascimento">
  <label for="dataNascimento" class="col-sm-6 control-label">Data de Nascimento</label>
  <div class="col-sm-2">
    <input type="text" data-mask='date' class="form-control" name="dataNascimento" id="dataNascimento" placeholder="dd/mm/aaaa" required>
  </div>
</div>

<div class="form-group">
  <label for="fumante" class="col-sm-6 control-label">Fumante</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="sim" id="sim" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="nao" id="nao" value="Não"> Não
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="Profissão" class="col-sm-6 control-label">Profissão</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="profissao" name="profissao" required>
  </div>
</div>

<div class="form-group">
  <label for="rendaMensal" class="col-sm-6 control-label">Renda Mensal</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="rendaMensal" name="RendaMensal" required>
  </div>
</div>


<div class="form-group cep">
  <label for="cep" class="col-sm-6 control-label">CEP</label>
  <div class="col-sm-6">
    <div class="row">
      <div class="col-sm-4">
        <input type="text" data-mask="cep" class="form-control" name="cep" id="cep" placeholder="CEP">
      </div>
      <h5 class="col-sm-8">
        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei meu CEP</a>
      </h5>
      <h5 class="col-sm-12 cep-result"></h5>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="cpfSegurado" class="col-sm-6 control-label">CPF do segurado</label>
  <div class="col-sm-4">
    <input type="text" data-mask="cpf" class="form-control" name="cpfSegurado" id="cpfSegurado" placeholder="">
  </div>
</div>

<div class="form-group">
  <label for="email" class="col-sm-6 control-label">E-mail do Segurado</label>
  <div class="col-sm-4">
    <input type="email" class="form-control" id="email" name="email" required>
  </div>
</div>