<h2 class="text-center"><strong>Agora, me conta como você utiliza o carro?</strong></h2>
<input type="hidden" name="section" value="Agora, me conta como você utiliza o carro?">
<br>
<div class="form-group">
  <label for="kmMediaMes" class="col-sm-6 control-label">Quilômetros rodados, em média, por mês:</label>
  <div class="col-sm-4">
		<select class="form-control" name="kmMediaMes" required>
			<option value="500">Até 500 km/mês</option>
			<option value="670">501 a 670</option>
			<option value="830">671 a 830</option>
			<option value="1250">831 a 1.250</option>
			<option value="1500">1.251 a 1.500</option>
			<option value="1670">1.501 a 1.670</option>
			<option value="2080">1.671 a 2.080</option>
			<option value="2500">2.081 a 2.500</option>
			<option value="3000">2.501 a 3.000</option>
			<option value="3100">Acima de 3.000</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label for="usoParaFaculdade" class="col-sm-6 control-label">O veículo é utilizado para ir a escola/faculdade/pós?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="usoParaFaculdade" id="usoParaFaculdade" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="usoParaFaculdade" id="usoParaFaculdade" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="usoParaTrabalho" class="col-sm-6 control-label">O veículo é utilizado para ida ao trabalho?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="usoParaTrabalho" id="usoParaTrabalho" value="Sim" required> Sim
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="usoParaTrabalho" id="usoParaTrabalho" value="Não"> Não
			</label>
		</div>
  </div>
</div>

<div class="form-group distanciaResidenciaTrabalho hide">
  <label for="distanciaResidenciaTrabalho" class="col-sm-6 control-label">Qual é a distância da residência do asdsa até o local de trabalho?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="distanciaResidenciaTrabalho" id="distanciaResidenciaTrabalho" value="10" required> Até 10km
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="distanciaResidenciaTrabalho" id="distanciaResidenciaTrabalho" value="20"> 10 a 20
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="distanciaResidenciaTrabalho" id="distanciaResidenciaTrabalho" value="30"> 20 a 30
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="distanciaResidenciaTrabalho" id="distanciaResidenciaTrabalho" value="40"> 30 a 40
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="distanciaResidenciaTrabalho" id="distanciaResidenciaTrabalho" value="50"> Acima de 40km
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="usoParaTrabalhar" class="col-sm-6 control-label">O veículo é utilizado como instrumento de trabalho?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="usoParaTrabalhar" id="usoParaTrabalhar" value="Não" required> Não
			</label>
			<div class="clearfix"></div>
			<label class="btn btn-default">
			  <input type="radio" name="usoParaTrabalhar" id="usoParaTrabalhar" value="Sim - Para realizar entregas"> Sim - Para realizar entregas
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="usoParaTrabalhar" id="usoParaTrabalhar" value="Sim - Para visitar clientes ou fornecedores"> Sim - Para visitar clientes ou fornecedores
			</label>
		</div>
  </div>
</div>

<div class="form-group ">
  <label for="possuiGaragemResidencia" class="col-sm-6 control-label">Possui garagem na residência (ou pernoite)? Como ela é?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="possuiGaragemResidencia" id="possuiGaragemResidencia" value="Sim, com portão manual" required> Sim, com portão manual
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemResidencia" id="possuiGaragemResidencia" value="Sim, com portão automático ou com porteiro"> Sim, com portão automático ou com porteiro
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemResidencia" id="possuiGaragemResidencia" value="Sim, em estacionamento privado pago ou fechado"> Sim, em estacionamento privado pago ou fechado
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemResidencia" id="possuiGaragemResidencia" value="Não, o carro fica na rua"> Não, o carro fica na rua
			</label>
		</div>
  </div>
</div>

<div class="form-group possuiGaragemFaculdade hide">
  <label for="possuiGaragemFaculdade" class="col-sm-6 control-label">Possui garagem na escola/faculdade/pós? Como ela é?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="possuiGaragemFaculdade" id="possuiGaragemFaculdade" value="Sim, com portão manual" required> Sim, com portão manual
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemFaculdade" id="possuiGaragemFaculdade" value="Sim, com portão automático ou com porteiro"> Sim, com portão automático ou com porteiro
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemFaculdade" id="possuiGaragemFaculdade" value="Sim, em estacionamento privado pago ou fechado"> Sim, em estacionamento privado pago ou fechado
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemFaculdade" id="possuiGaragemFaculdade" value="Não, o carro fica na rua"> Não, o carro fica na rua
			</label>
		</div>
  </div>
</div>

<div class="form-group possuiGaragemTrabalho hide">
  <label for="possuiGaragemTrabalho" class="col-sm-6 control-label">Possui garagem no trabalho? Como ela é? </label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="possuiGaragemTrabalho" id="possuiGaragemTrabalho" value="Sim, com portão manual" required> Sim, com portão manual
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemTrabalho" id="possuiGaragemTrabalho" value="Sim, com portão automático ou com porteiro"> Sim, com portão automático ou com porteiro
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemTrabalho" id="possuiGaragemTrabalho" value="Sim, em estacionamento privado pago ou fechado"> Sim, em estacionamento privado pago ou fechado
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="possuiGaragemTrabalho" id="possuiGaragemTrabalho" value="Não, o carro fica na rua"> Não, o carro fica na rua
			</label>
		</div>
  </div>
</div>

<div class="form-group cepEstacionadoNoite">
  <label for="cepEstacionadoNoite" class="col-sm-6 control-label">Qual é o CEP onde o veículo é estacionado à noite? </label>
  <div class="col-sm-6">
  	<div class="row">
	  	<div class="col-sm-4">
				<input type="text" data-mask="cep" class="form-control" name="cepEstacionadoNoite" id="cepEstacionadoNoite" placeholder="CEP" required>
			</div>
			<h5 class="col-sm-8">
				<a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei meu CEP</a>
			</h5>
			<h5 class="col-sm-12 cep-result"></h5>
		</div>
  </div>
</div>