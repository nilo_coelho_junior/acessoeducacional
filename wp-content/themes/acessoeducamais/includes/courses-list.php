<div class="<?php if (is_home()) { ?> container container-wrapper <?php } else { ?> container-fluid <?php } ?>">
  <h2 class="page-title text-center">Escolha seu curso</h2>
  <div class="center-block">
    <ul class="nav nav-pills center-nav" role="tablist">
      <?php
        // get pos graduacoes
        $query = new WP_Query(array('post_type' => 'ensino', 'order' => 'menu_order'));

        if ($query->have_posts()):
          while($query->have_posts()) : $query->the_post();
            $slug = basename(get_permalink());
      ?>
            <li role="presentation" class="<?php echo $query->current_post == 0 ? 'active' : '';?> text-uppercase">
              <a href="#<?php echo $slug;?>" aria-controls="<?php echo $slug;?>" role="tab" data-toggle="tab">
                <?php the_title(); ?>
              </a>
            </li>
      <?php
          endwhile;
        endif;
      ?>
    </ul>

    <div class="tab-content">
      <?php
        // a quantidade de cursos por categoria
        if ($query->have_posts()):
          while($query->have_posts()) : $query->the_post();
            $post_id = get_the_id();
            $curses_id = new WP_Query(array('post_type' => 'curso', 'meta_query' => array(array('key' => '_wpcf_belongs_ensino_id', 'value' => $post_id)), 'fields' => 'ids'));
            $curses = types_child_posts('curso');
            $categories = get_categories(array(
              'taxonomy' => 'tipo',
              'object_ids' => $curses_id->posts,
              'hide_empty' => true,
              'orderby' => 'name'
            ));
    ?>
            <div role="tabpanel" class="tab-pane <?php echo $query->current_post == 0 ? 'active fade in' : 'fade';?>" id="<?php echo basename(get_permalink());?>" >
    <?php
              if (empty($categories) && !empty($curses)){
    ?>
                <ul class="list-arrow">
    <?php
                  foreach ($curses as $curse) {
    ?>
                    <li class="col-xs-6">
                      <a href="<?php echo get_permalink($curse->ID); ?>">
                      <?php echo($curse->post_title); ?>
                      </a>
                    </li>
    <?php
                  }
    ?>
                </ul>
    <?php
              }else{
    ?>
                <ul class="nav nav-pills center-nav simple-nav" role="tablist">
    <?php
                  $cat_count = 0;
                  foreach ($categories as $cat) {

    ?>
                    <li role="presentation" class="<?php echo $cat_count == 0 ? 'active' : '';?> text-uppercase">
                      <a href="#<?php echo $cat->slug;?>" aria-controls="<?php echo $cat->slug;?>" role="tab" data-toggle="tab">
                        <?php echo $cat->cat_name; ?>
                      </a>
                    </li>
    <?php
                    $cat_count++;
                  }
    ?>
                </ul>
                <div class="tab-content">
    <?php
                  $cat_count = 0;
                  foreach ($categories as $cat) {
    ?>
                    <div role="tabpanel" class="tab-pane <?php echo $cat_count == 0 ? 'active fade in' : 'fade';?>" id="<?php echo $cat->slug;?>" >
    <?php
                      $courses_cat = new WP_Query(array(
                        'post_type' => 'curso',
                        'tax_query' => array(array(
                          'taxonomy' => 'tipo',
                          'terms' => array($cat->term_id))),
                        'meta_query' => array(array(
                          'key' => '_wpcf_belongs_ensino_id',
                          'value' => $post_id)),
                        'orderby' => 'title',
                        'order' => 'ASC'
                      ));

                      if ($courses_cat->have_posts()):
    ?>
                        <ul class="list-arrow">
    <?php
                          while($courses_cat->have_posts()) : $courses_cat->the_post();
    ?>
                            <li class="col-xs-6">
                              <a href="<?php echo the_permalink(); ?>">
                                <?php echo(the_title()); ?>
                              </a>
                            </li>
    <?php
                          endwhile;
    ?>
                        </ul>
    <?php
                      endif;
    ?>
                    </div>
    <?php
                    $cat_count++;
                  }
    ?>
                </div>
    <?php
              }
    ?>
            </div>
    <?php
          endwhile;
        endif;
      ?>

    </div>

  </div>
</div>
