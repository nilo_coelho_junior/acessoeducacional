<?php

function theme_settings_page(){
?>
      <div class="wrap">
      <h1>Configurações</h1>
      <form method="post" action="options.php">
          <?php
              settings_fields("section");
              do_settings_sections("theme-options");
              submit_button();
          ?>
      </form>
    </div>
  <?php
}

function display_office_hours_element(){
  ?>
      <input type="text" name="office_hours" id="office_hours" value="<?php echo get_option('office_hours'); ?>" placeholder="Segunda a Sexta das 8h às 17h, Sábados 8h às 12h"/>
    <?php
}

function display_whatsapp_element(){
  ?>
      <input type="text" name="whatsapp" id="whatsapp" value="<?php echo get_option('whatsapp'); ?>" />
    <?php
}

function display_contact_number_element(){
  ?>
      <input type="text" name="contact_number" id="contact_number" value="<?php echo get_option('contact_number'); ?>" />
    <?php
}

function display_instagram_element(){
  ?>
      <input type="text" name="instagram_url" id="instagram_url" value="<?php echo get_option('instagram_url'); ?>" />
    <?php
}

function display_facebook_element(){
  ?>
      <input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
    <?php
}

function display_maps_element(){
  ?>
      <input type="text" name="maps_iframe" id="maps_iframe" value="<?php echo htmlspecialchars(get_option('maps_iframe')); ?>" />
    <?php
}

function display_theme_panel_fields(){
  add_settings_section("section", "All Settings", null, "theme-options");

  add_settings_field("office_hours", "Horário de Atendimento", "display_office_hours_element", "theme-options", "section");
  add_settings_field("whatsapp", "Whatsapp", "display_whatsapp_element", "theme-options", "section");
  add_settings_field("contact_number", "Telefone de Contato", "display_contact_number_element", "theme-options", "section");
  add_settings_field("instagram_url", "Instagram Profile Url", "display_instagram_element", "theme-options", "section");
  add_settings_field("facebook_url", "Facebook Profile Url", "display_facebook_element", "theme-options", "section");
  add_settings_field("maps_iframe", "Google Maps Iframe", "display_maps_element", "theme-options", "section");

  register_setting("section", "office_hours");
  register_setting("section", "whatsapp");
  register_setting("section", "contact_number");
  register_setting("section", "instagram_url");
  register_setting("section", "facebook_url");
  register_setting("section", "maps_iframe");
}

add_action("admin_init", "display_theme_panel_fields");


function add_theme_menu_item(){
  add_menu_page("Theme Panel", "Theme Panel", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

?>
