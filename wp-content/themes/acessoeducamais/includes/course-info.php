
<!-- <h3 class="no-margin"><strong><?php #echo the_title(); ?></strong></h3> -->
<br>
<div>
  <!-- <ul class="nav nav-pills nav-amaranth nav-amaranth-light">
    <li role="presentation" class="active">
      <a href="#apresentacao" aria-controls="apresentacao" role="tab" data-toggle="tab">Apresentação</a>
    </li>
    <li role="presentation">
      <a href="#objetivos" aria-controls="objetivos" role="tab" data-toggle="tab">Objetivo do Curso</a>
    </li>
    <li role="presentation">
      <?php if ($metodologia[0] == 'pdf') { ?>
        <a href="<?php echo $metodologia[1] ?>" target='_blank'>Metodologia</a>
      <?php } else { ?>
        <a href="#metodologia" aria-controls="metodologia" role="tab" data-toggle="tab">Metodologia</a>
      <?php } ?>
    </li>
    <li role="presentation">
      <a href="#avaliacoes" aria-controls="avaliacoes" role="tab" data-toggle="tab">Avaliações de Aprendizagem</a>
    </li>
    <li role="presentation">
      <a href="#investimento" aria-controls="investimento" role="tab" data-toggle="tab">Investimento</a>
    </li>
  </ul> -->

  <!-- Tab panes -->
  <div class="tab-content">
    <br>
    <div role="tabpanel" class="tab-pane active" id="apresentacao">
      <?php echo get_curso_field('apresentacao', get_the_ID(), $category[0]->term_id); ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="objetivos">
      <?php echo get_curso_field('objetivo-do-curso', get_the_ID(), $category[0]->term_id); ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="metodologia">
      <?php echo $metodologia[1]; ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="avaliacoes">
      <?php echo get_curso_field('avaliacao-de-aprendizagem', get_the_ID(), $category[0]->term_id); ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="investimento">
      <?php echo get_curso_field('investimento', get_the_ID(), $category[0]->term_id); ?>
    </div>
  </div>

</div>
