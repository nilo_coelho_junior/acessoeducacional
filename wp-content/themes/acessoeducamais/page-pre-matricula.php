<?php get_header(); ?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title"><?php the_title(); ?></h2>

    <form action="" class="pre-enrollment-form form-horizontal">
      <?php wp_nonce_field('send_pre_enrollment', 'token'); ?>

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
          <a href="#first" aria-controls="first" role="tab" data-toggle="tab">
              <span class="visible-sm visible-xs">1</span>
              <span class="hidden-sm hidden-xs">Escolha seu Curso</span>
          </a>
        </li>
        <li role="presentation">
          <a href="#second" aria-controls="second" role="tab" data-toggle="tab">
              <span class="visible-sm visible-xs">2</span>
              <span class="hidden-sm hidden-xs">Informe seus Dados</span>
          </a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content container">
        <div role="tabpanel" class="tab-pane active" id="first">
          <?php
            if ( have_posts() ) : while ( have_posts() ) : the_post();
              echo the_content();
            endwhile; endif;
          ?>
          <br>
          <div class="form-group">
            <label for="tipo_curso" class="col-sm-2 control-label">Tipo do Curso</label>
            <div class="col-sm-4">
              <select class="form-control tipo-curso" name="ensino" required>
                <option selected disabled="">Selecione um tipo de curso</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="curso" class="col-sm-2 control-label">Curso</label>
            <div class="col-sm-6">
              <select class="form-control cursos" name="curso" required>
                <option selected disabled="">Selecione um curso</option>
              </select>
            </div>
          </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="second">
          <div class="col-sm-12 col-sm-pull-2">
            <div class="form-group">
              <label for="name" class="col-sm-6 control-label">Nome</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="name" required>
              </div>
            </div>

            <div class="form-group">
              <label for="email" class="col-sm-6 control-label">E-mail</label>
              <div class="col-sm-6">
                <input type="email" class="form-control" name="email" required>
              </div>
            </div>

            <div class="form-group">
              <label for="state" class="col-sm-6 control-label">Estado</label>
              <div class="col-sm-6">
                <select class="form-control" name="state" required>
                  <option selected disabled>Selecione um Estado</option>
                  <?php
                    $states = new WP_Query(array('post_type' => 'estado', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC'));
                    while($states->have_posts()) : $states->the_post();
                  ?>
                    <option value="<?php echo the_ID(); ?>"><?php echo the_title(); ?></option>
                  <?php
                    endwhile;
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="city" class="col-sm-6 control-label">Cidade</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="city" required>
              </div>
            </div>

            <div class="form-group">
              <label for="phone" class="col-sm-6 control-label">Telefone para contato</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="phone" data-mask="phone" required>
              </div>
            </div>

            <div class="form-group">
              <label for="phoneType" class="col-sm-6 control-label">Tipo</label>
              <div class="col-sm-6">
                <select class="form-control" name="phoneType" required>
                  <option value=""></option>
                  <option value="fixo">Fixo</option>
                  <option value="claro">Claro</option>
                  <option value="nextel">Nextel</option>
                  <option value="oi">Oi</option>
                  <option value="tim">Tim</option>
                  <option value="vivo">Vivo</option>
                  <option value="outra">Outra</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="phone" class="col-sm-6 control-label">Como conheceu o site?</label>
              <div class="col-sm-6">
                <label class="checkbox-inline">
                  <input type="checkbox" id="google" value="google"> Google
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="facebook" value="facebook"> Facebook
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="instagram" value="instagram"> Instagram
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="amigos" value="amigos"> Amigos
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="outra" value="outra"> Outra
                </label>
              </div>
            </div>

            <br>
          </div>

        </div>
        <div class="container">
          <br>
          <div class="col-lg-6 text-right">
            <button class="btn btn-default btn-lg previous" type="button">Voltar</button>
          </div>
          <div class="col-lg-6">
            <button class="btn btn-primary btn-lg next" type="button">Ótimo! Próximo passo</button>
          </div>
        </div>
      </div>
      <br>
    </form>
  </div>
</div>

<?php get_footer(); ?>
