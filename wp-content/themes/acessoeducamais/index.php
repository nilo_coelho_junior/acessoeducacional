<?php
  global $wp;
  $custom_post = $wp->request;
  if ($custom_post == 'polo') {
    header('Location: ' . get_page_link(get_page_by_path('polos')), true, 303);
    exit();
  } else if ($custom_post == 'curso' || $custom_post == 'ensino') {
    header('Location: ' . get_page_link(get_page_by_path('cursos')), true, 303);
  } else {
    get_header();
?>
    <div class="container-fluid">
      <div class="container">
        <h2 class="page-title">Blog</h2>
        <div class="widget_siteorigin-panels-postloop">
          <?php get_template_part( 'loop'); ?>
        </div>
      </div>
    </div>

<?php
    get_footer();
  }
?>
