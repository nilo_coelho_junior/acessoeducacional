<?php
$states = get_posts( array('post_type' => 'estado', 'orderby' => 'title', 'nopaging' => true) );
$states_option = [];

foreach ($states as $state) {
  $polos = new WP_Query(array('post_type' => 'polo', 'meta_query' => array(array('key' => '_wpcf_belongs_estado_id', 'value' => $state->ID))));
  if ( $polos->have_posts() ) {
    $polos = new WP_Query(array('post_type' => 'polo', 'meta_query' => array(array('key' => '_wpcf_belongs_estado_id', 'value' => $state->ID))));
    array_push($states_option, [$state, $polos->get_posts()]);
  }
}

?>

<?php get_header(); ?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title"><?php the_title(); ?></h2>
  </div>

  <div class="container">
    <?php
      if ( have_posts() ) : while ( have_posts() ) : the_post();
        echo the_content();
      endwhile; endif;
    ?>
  </div>

  <div class="container">
    <hr>
    <form class="form-inline">
      <div class="form-group">
        <label for="filter-polos"> Selecione o Polo: </label>
        <select id="filter-polos" class="form-control col-sm-6">
          <option value="all" selected disabled>&nbsp;</option>
          <?php
            foreach ($states_option as $item) {
          ?>
              <optgroup label="<?php echo $item[0]->post_title; ?>">
                <?php
                  foreach ($item[1] as $options) {
                  ?>
                    <option value="<?php echo $options->post_title; ?>"><?php echo $options->post_title; ?></option>
                  <?php
                  }
                ?>
              </optgroup>
          <?php
            }
          ?>
        </select>
      </div>
    </form>
    <hr>
  </div>

  <div class="container-fluid">
    <?php
      foreach ($states as $state) {
      $polos = new WP_Query(array('post_type' => 'polo', 'meta_query' => array(array('key' => '_wpcf_belongs_estado_id', 'value' => $state->ID))));
      if ( $polos->have_posts() ) {
    ?>
      <div class="container container-polos" style="display: none;">
        <h3><strong><?php echo $state->post_title; ?></strong></h3>
        <br>
    <?php
        while ( $polos->have_posts() ) : $polos->the_post();
    ?>
        <div class="panel panel-amaranth panel-default panel-polo">
          <div class="panel-heading panel-heading-polo"><?php  echo the_title(); ?></div>
          <div class="panel-body row">
            <div class="col-sm-12">
              <div class="col-sm-6 polo-informacoes">
                <!-- <h4><strong>Informações</strong></h4> -->
                <!-- <hr> -->
                <br>
                <p>
                  Início das aulas: <br>
                  <strong><?php echo date_i18n('d \d\e F \d\e Y', get_post_meta(get_the_ID(), 'wpcf-inicio-das-aulas', true)); ?></strong>
                </p>

                <p>
                  Período de Matrículas: <br>
                  <strong><?php echo get_post_meta(get_the_ID(), 'wpcf-periodo-de-matriculas-abertas', true); ?></strong>
                </p>

                <?php
                  $email = get_post_meta(get_the_ID(), 'wpcf-polo-e-mail', true);
                  if (!empty($email)) {
                ?>
                  <p>
                    E-mail: <br>
                    <strong><?php echo $email; ?></strong>
                  </p>
                <?php } ?>

                <?php
                  $telefones = get_post_meta(get_the_ID(), 'wpcf-telefones', true);
                  if (!empty($telefones)) {
                ?>
                  <p>
                    Telefones: <br>
                    <strong><?php echo $telefones; ?></strong>
                  </p>
                <?php } ?>
                <?php echo nl2br(get_post_meta(get_the_ID(), 'wpcf-endereco-polo', true)); ?>
                <br><br>
              </div>
              <div class="col-sm-6 no-pd">
                <?php
                  $iframe = get_post_meta( get_the_ID(), 'wpcf-google-maps-iframe', true );
                  $url = explode('" width="', explode('src="', $iframe)[1])[0];
                ?>
                <iframe src="<?php echo $url; ?>" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="col-sm-12">
                <hr>
                <h4><strong>Eventos</strong></h4>
                <br>
                <?php
                  $eventos = new WP_Query(array('post_type' => 'evento', 'meta_query' => array(array('key' => '_wpcf_belongs_polo_id', 'value' => get_the_ID()))));
                  if ( !$eventos->have_posts() ) {
                ?>
                    <p>Não exite eventos para o polo.</p>
                <?php
                  } else {
                ?>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Data</th>
                          <th>Detalhes</th>
                        </tr>
                      </thead>
                      <tbody>
                <?php
                    while ( $eventos->have_posts() ) : $eventos->the_post();
                      $date_start = date_i18n('d/m/Y', get_post_meta(get_the_ID(), 'wpcf-data-de-inicio-do-evento', true));
                      $date_end = date_i18n('d/m/Y', get_post_meta(get_the_ID(), 'wpcf-data-de-encerramento-do-evento', true));
                      $date = $date_start;
                      if (!empty($date_end) && $date_end != $date_start){
                        $date = 'de: '.$date.' até '.$date_end;
                      }
                ?>
                      <tr>
                        <th><?php echo $date; ?></th>
                        <td>
                          <?php echo the_title(); ?>
                          <br><br>
                          <?php echo the_content(); ?>
                        </td>
                      </tr>
                <?php
                    endwhile;
                ?>
                      </tbody>
                    </table>
                <?php
                  }
                ?>
              </div>
            </div>
          </div>
        </div>
    <?php
        endwhile;
        wp_reset_postdata();
    ?>
      </div>
    <?php
        }
      }
    ?>
  </div>

</div>

<?php
  wp_reset_postdata();
  get_footer();
?>
