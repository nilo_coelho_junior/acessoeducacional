    <div id="footer" class="container-fluid wrapper footer">
      <div class="container text-center">
        <img src="<?php header_image(); ?>" alt=""/>
      </div>
    </div>

    <?php wp_footer(); ?>

  </div>
</body>
