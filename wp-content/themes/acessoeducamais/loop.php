<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
  <div id="post-0" class="post error404 not-found">
    <h1 class="entry-title"><?php echo('Nada encontrado'); ?></h1>
    <div class="entry-content">
      <p><?php _e( 'Desculpe, não pudemos encontrar a página que procurava. Tente uma nova busca.'); ?></p>
      <?php get_search_form(); ?>
    </div><!-- .entry-content -->
  </div><!-- #post-0 -->
<?php endif; ?>

<?php
  /*
   * Start the Loop.
   *
   * In Twenty Ten we use the same loop in multiple contexts.
   * It is broken into three main parts: when we're displaying
   * posts that are in the gallery category, when we're displaying
   * posts in the asides category, and finally all other posts.
   *
   * Additionally, we sometimes check for whether we are on an
   * archive page, a search page, etc., allowing for small differences
   * in the loop on each template without actually duplicating
   * the rest of the loop that is shared.
   *
   * Without further ado, the loop:
   */
  $contentClass = (is_search()) ? 'col-sm-6' : 'col-sm-4' ;
?>
<div class="grid" data-masonry='{"itemSelector": ".grid-item", "columnWidth": ".grid-sizer", "percentPosition": true}'>
  <div class="grid-sizer <?php echo $contentClass; ?>"></div>
  <?php
    while ( have_posts() ) : the_post();
  ?>
      <div id="post-<?php the_ID(); ?>" <?php post_class($contentClass.' grid-item'); ?>>
        <div class="wrapper-post">
          <?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
            <div class="post-thumbnail">
              <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail(array(500, 250)); ?>
              </a>
            </div><!-- .post-thumbnail -->
          <?php endif; ?>

          <div class="preview-post">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
            <div class="meta">
              <p class="categories-links">
                <?php printf('%2$s', 'entry-utility-prep entry-utility-prep-cat-links', get_the_category_list( ', ' ) ); ?>
              </p>
            </div>
            <div class="entry-meta">
              <?php //twentyten_posted_on(); ?>
            </div><!-- .entry-meta -->
            <div class="entry-content">
              <a href="<?php get_permalink();?>">
                <?php excerpt_max_charlength(); ?>
              </a>
              <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
            </div><!-- .entry-content -->
          </div>
        </div>
      </div><!-- #post-## -->

      <?php comments_template( '', true ); ?>
  <?php endwhile; // End the loop. Whew. ?>
</div>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if ( $wp_query->max_num_pages > 1 ) : ?>
  <nav aria-label="Page navigation">
    <ul class="pagination col-sm-12">
      <li class="pull-left">
        <?php previous_posts_link('<span class="meta-nav">&laquo;</span> Posts Recentes'); ?>
      </li>
      <li class="pull-right">
        <?php next_posts_link('Posts antigos <span class="meta-nav">&raquo;</span>', 0); ?>
      </li>
    </ul>
  </nav>
<?php endif; ?>
