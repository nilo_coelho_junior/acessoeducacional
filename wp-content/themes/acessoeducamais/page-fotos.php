<?php get_header(); ?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title"><?php the_title(); ?></h2>
  </div>

  <div class="container">
    <?php
      if ( have_posts() ) : while ( have_posts() ) : the_post();
        echo the_content();
      endwhile; endif;
    ?>
  </div>
  <br>
  <div class="container">
    <?php
      $fotos = new WP_Query(array('post_type' => 'foto'));
      $contentClass = 'col-sm-4' ;
    ?>

    <div class="grid row" data-masonry='{"itemSelector": ".grid-item", "columnWidth": ".grid-sizer", "percentPosition": true}'>
      <div class="grid-sizer <?php echo $contentClass; ?>"></div>
      <?php
        while ( $fotos->have_posts() ) : $fotos->the_post();
      ?>
          <div id="post-<?php the_ID(); ?>" <?php post_class($contentClass.' grid-item'); ?>>
            <div class="wrapper-post">
              <?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
                <div class="post-thumbnail">
                  <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail(array(500, 250)); ?>
                  </a>
                </div><!-- .post-thumbnail -->
              <?php endif; ?>

              <div class="preview-post">
                <div class="meta">
                  <span><?php the_time('j \d\e F \d\e Y'); ?></span>
                </div>
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
                <div class="entry-meta">
                  <?php //twentyten_posted_on(); ?>
                </div><!-- .entry-meta -->
                <div class="entry-content">
                  <a href="<?php get_permalink();?>">
                    <?php excerpt_max_charlength(); ?>
                  </a>
                  <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                </div><!-- .entry-content -->
              </div>
            </div>
          </div><!-- #post-## -->

          <?php # comments_template( '', true ); ?>
      <?php endwhile; // End the loop. Whew. ?>
    </div>
    <?php if ( $wp_query->max_num_pages > 1 ) : ?>
      <nav aria-label="Page navigation">
        <ul class="pagination col-sm-12">
          <li class="pull-left">
            <?php previous_posts_link('<span class="meta-nav">&laquo;</span> Posts Recentes'); ?>
          </li>
          <li class="pull-right">
            <?php next_posts_link('Posts antigos <span class="meta-nav">&raquo;</span>', 0); ?>
          </li>
        </ul>
      </nav>
    <?php endif; ?>
  </div>
</div>

<?php get_footer(); ?>
