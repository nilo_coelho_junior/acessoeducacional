<?php

// function theme_postsbycategory() {
//   $post_id = get_the_id();
//   print_r(wp_get_post_categories($post_id));
//   // Defino o Nome da Categoria e a Quantidade de Posts a serem exibidos
//   $the_query = new WP_Query( array( 'category__in' => wp_get_post_categories($post_id), 'posts_per_page' => 4 ) );

//   // O Loop
//   if ( $the_query->have_posts() ) {
//     $string .= '<ul class="postsbycategory widget_recent_entries">';
//     while ( $the_query->have_posts() ) {
//       $the_query->the_post();
//       if ( has_post_thumbnail() ) {
//         $string .= '<li>';
//         $string .= '<a href="' . get_the_permalink() .'" rel="bookmark">' . get_the_post_thumbnail($post_id, array( 50, 50) ) . get_the_title() .'</a></li>';
//       } else {
//         // Se nenhuma imagem de destaque foi cadastrada, exibe apenas o título do post
//         $string .= '<li><a href="' . get_the_permalink() .'" rel="bookmark">' . get_the_title() .'</a></li>';
//       }
//     }
//   } else {
//   // Nada será exibido se nenhum Post Relacionado por Categoria foi encontrado
//   }
//   $string .= '</ul>';

//   return $string;

//   /* Restaura os dados originais do post */
//   wp_reset_postdata();
// }

// Adiciona um Shortcode
// add_shortcode('categoryposts', 'clwp_postsbycategory');

add_image_size( 'custom-size', 500, 600, true );

// Ativa shortcodes em Widgets de Texto
add_filter('widget_text', 'do_shortcode');


function custom_excerpt_length( $length ) {
    return 200;
}
add_filter( 'excerpt_length', 'custom_excerpt_length');

function excerpt_max_charlength($charlength = 200) {
  $excerpt = get_the_excerpt();
  $charlength++;

  if ( mb_strlen( $excerpt ) > $charlength ) {
    $subex = mb_substr( $excerpt, 0, $charlength - 5 );
    $exwords = explode( ' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      echo mb_substr( $subex, 0, $excut );
    } else {
      echo $subex;
    }
    echo '[...]';
  } else {
    echo $excerpt;
  }
}

/**
 * Register widget area.
 */
function acessoeducacional_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Widget Area', 'twentyfifteen' ),
    'id'            => 'sidebar',
    'description'   => 'Adicione widgets aqui para exibir-los na sidebar.',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'acessoeducacional_widgets_init' );

function the_slug($echo=true){
  $slug = basename(get_permalink());
  do_action('before_slug', $slug);
  $slug = apply_filters('slug_filter', $slug);
  do_action('after_slug', $slug);
  return $slug;
}

function aem_parse_request( $query ) {
  // print_r($query);
  if ( ! $query->is_main_query() )
    return;

  // Only noop our very specific rewrite rule match
  if ( 2 != count( $query->query ) ) {
    return;
  }


  // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
  if ( ! empty( $query->query['name'] ) ) {
    $tipodecurso = new WP_Query(array('post_type' => 'tipodecurso', 'order' => 'menu_order'));
    $post_is_tipodecurso = false;
    if ($tipodecurso->have_posts()){
      while($tipodecurso->have_posts()) : $tipodecurso->the_post();
        if (the_slug() == $query->query['name']){
          $post_is_tipodecurso = true;
          $query->set( 'post_type', array( 'post', 'tipodecurso', 'page' ) );
          break;
        }
      endwhile;
    }

    if (!$post_is_tipodecurso){
      if ( $query->query['name'] == 'blog' ) {
        $query->set( 'posts_per_page', 10 );
      }
    }

  }
  // return $query;
}
// add_action( 'pre_get_posts', 'aem_parse_request' );

//Page Slug Body Class
function add_slug_body_class( $classes ) {
  global $post;
  if ( isset( $post ) ) {
    $classes[] = $post->post_type . '-' . $post->post_name;
  }
  return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

function mailster_get_pre_matricula($id) {
  $user = mailster( 'subscribers' )->get( $id );
  $query = new WP_Query(array(
    'post_type' => 'post-pre-matricula',
    'meta_query' => array(array(
      'key' => 'wpcf-email',
      'value' => $user->email
    )),
    'order' => 'DESC',
    'posts_per_page' => 1
  ));
  return $query->posts[0];
}

function mailster_get_polo($pre_id){
  $state = get_post_meta($pre_id, '_wpcf_belongs_estado_id', true);
  $query = new WP_Query(array(
    'post_type' => 'polo',
    'meta_query' => array(array(
      'key' => '_wpcf_belongs_estado_id',
      'value' => $state
    )),
    'order' => 'DESC',
    'posts_per_page' => 1
  ));
  return $query->posts[0];
}

function mailster_get_ensino($pre_id) {
  return get_post_meta($pre_id, '_wpcf_belongs_ensino_id', true);
}

function mailster_get_curso($pre_id) {
  return get_post_meta($pre_id, '_wpcf_belongs_curso_id', true);
}

if ( function_exists( 'mailster_add_tag' ) ) {
  function add_tag_ensino_function($option, $fallback, $campaignID = NULL, $subscriberID = NULL){
    $pre = mailster_get_pre_matricula($subscriberID);
    $ensino = mailster_get_ensino($pre->ID);
    return $ensino->post_title;
  }
  mailster_add_tag('Ensino', 'add_tag_ensino_function');

  function add_tag_curso_function($option, $fallback, $campaignID = NULL, $subscriberID = NULL){
    $pre = mailster_get_pre_matricula($subscriberID);
    $curso = mailster_get_curso($pre->ID);
    return $curso->post_title;
  }
  mailster_add_tag('Curso', 'add_tag_curso_function');

  function add_tag_data_de_recebimento_function($option, $fallback, $campaignID = NULL, $subscriberID = NULL){
    $pre = mailster_get_pre_matricula($subscriberID);
    $polo = mailster_get_polo($pre->ID);
    $data = get_post_meta($polo->ID, 'wpcf-data-de-recebimento', true);
    return date_i18n('d/m/Y', $data);
  }
  mailster_add_tag('Data de recebimento', 'add_tag_data_de_recebimento_function');

  function add_tag_data_de_início_function($option, $fallback, $campaignID = NULL, $subscriberID = NULL){
    $pre = mailster_get_pre_matricula($subscriberID);
    $polo = mailster_get_polo($pre->ID);
    $data = get_post_meta($polo->ID, 'wpcf-inicio-das-aulas', true);
    return date_i18n('d/m/Y', $data);
  }
  mailster_add_tag('Data de início', 'add_tag_data_de_início_function');

  function add_tag_data_primeiro_cheque_function($option, $fallback, $campaignID = NULL, $subscriberID = NULL){
    $pre = mailster_get_pre_matricula($subscriberID);
    $polo = mailster_get_polo($pre->ID);
    $data = get_post_meta($polo->ID, 'wpcf-data-do-primeiro-cheque', true);
    return date_i18n('d/m/Y', $data);
  }
  mailster_add_tag('Data 1º cheque', 'add_tag_data_primeiro_cheque_function');

  function add_tag_data_da_turma_function($option, $fallback, $campaignID = NULL, $subscriberID = NULL){
    $pre = mailster_get_pre_matricula($subscriberID);
    $polo = mailster_get_polo($pre->ID);
    $data = get_post_meta($polo->ID, 'wpcf-data-da-turma', true);
    return date_i18n('d/m/Y', $data);
  }
  mailster_add_tag('Data da turma', 'add_tag_data_da_turma_function');

  function add_tag_cidade_do_polo_function($option, $fallback, $campaignID = NULL, $subscriberID = NULL){
    $pre = mailster_get_pre_matricula($subscriberID);
    $polo = mailster_get_polo($pre->ID);
    return $polo->post_title;
  }
  mailster_add_tag('Cidade do Polo', 'add_tag_cidade_do_polo_function');
}

?>
