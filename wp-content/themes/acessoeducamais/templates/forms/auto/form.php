<?php 

global $parent;

?>

<!-- Nav tabs -->
<ul class="nav nav-tabs steps hide-for-small-only" role="tablist">
  <li role="presentation" class="active">
  	<a href="#first" aria-controls="first" role="tab" data-toggle="tab">
      	<span class="visible-sm visible-xs">1</span>
      	<span class="hidden-sm hidden-xs">Vamos Lá</span>
  	</a>
  </li>
  <li role="presentation" class="">
    <a href="#second" aria-controls="second" role="tab" data-toggle="tab">
        <span class="visible-sm visible-xs">2</span>
        <span class="hidden-sm hidden-xs">Dados do Segurado</span>
    </a>
  </li>
  <li role="presentation" style="display: none;">
    <a href="#third" aria-controls="third" role="tab" data-toggle="tab">
      <span class="visible-sm visible-xs">3</span>
      <span class="hidden-sm hidden-xs">Quem é o principal condutor?</span>
    </a>
  </li>
  <li role="presentation" class="">
    <a href="#fourth" aria-controls="fourth" role="tab" data-toggle="tab">
    	<span class="visible-sm visible-xs">4</span>
	    <span class="hidden-sm hidden-xs">Qual é o carro?</span>
    </a>
  </li>
  <li role="presentation" class="">
  	<a href="#fifth" aria-controls="fifth" role="tab" data-toggle="tab">
  		<span class="visible-sm visible-xs">5</span>
      <span class="hidden-sm hidden-xs">Como utiliza o carro?</span>
  	</a>
  </li>
</ul>
<!-- Tab panes -->
<?php get_template_part('templates/loading'); ?>
<div class="tab-content container">
  <div role="tabpanel" class="tab-pane active" id="first">
  	<?php get_template_part('templates/forms/'.$parent->post_name.'/vamos-comecar'); ?>
  </div>
  <div role="tabpanel" class="tab-pane" id="second">
    <?php get_template_part('templates/forms/'.$parent->post_name.'/segurado'); ?>
  </div>
  <div role="tabpanel" class="tab-pane" id="third">
  	<?php get_template_part('templates/forms/'.$parent->post_name.'/condutor'); ?>
  </div>
  <div role="tabpanel" class="tab-pane" id="fourth">
  	<?php get_template_part('templates/forms/'.$parent->post_name.'/veiculo'); ?>
  </div>
  <div role="tabpanel" class="tab-pane" id="fifth">
  	<?php get_template_part('templates/forms/'.$parent->post_name.'/uso-carro'); ?>
  </div>
  <div class="container">
		<div class="col-lg-6 text-right">
			<button class="btn btn-default btn-lg previous" type="button">Voltar</button>
		</div>
		<div class="col-lg-6">
			<button class="btn btn-primary btn-lg next">Ótimo! Próximo passo</button>
		</div>
	</div>
</div>
<br>