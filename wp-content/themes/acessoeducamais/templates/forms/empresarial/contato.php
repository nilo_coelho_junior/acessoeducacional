<h2 class="text-center"><strong>Informe os dados da sua empresa</strong></h2>
<br>

<div class="form-group">
  <label for="ramoAtuacao" class="col-sm-6 control-label">Ramo de atuação da empresa</label>
  <div class="col-sm-3">
    <select class="form-control" name="ramoAtuacao" required>
      <option selected disabled>Selecione uma área</option>
      <option value="Bares e Restaurantes">Bares e Restaurantes</option>
      <option value="Bares e Restaurantes">Buffets</option>
      <option value="Cafeterias e Docerias">Cafeterias e Docerias</option>
      <option value="Clínicas de Estéticas">Clínicas de Estéticas</option>
      <option value="Comércio e Serviços">Comércio e Serviços</option>
      <option value="Consultórios">Consultórios</option>
      <option value="Escolas">Escolas</option>
      <option value="Escritórios">Escritórios</option>
      <option value="Floriculturas">Floriculturas</option>
      <option value="Hotéis e Pousadas">Hotéis e Pousadas</option>
      <option value="Lavanderias">Lavanderias</option>
      <option value="Livrarias">Livrarias</option>
      <option value="Padarias e Confeitarias">Padarias e Confeitarias</option>
      <option value="Papelarias">Papelarias</option>
      <option value="Pet Shops">Pet Shops</option>
      <option value="Salão de Cabeleireiros">Salão de Cabeleireiros</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label for="tipoDeConstrucao" class="col-sm-6 control-label">Tipo de construção do seu empreendimento?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="tipoDeConstrucao" id="tipoDeConstrucao" value="Superior sólida (concreto armado)" required> Superior sólida (concreto armado)
      </label>
      <label class="btn btn-default">
        <input type="radio" name="tipoDeConstrucao" id="tipoDeConstrucao" value="Mista inferior (madeira e concreto)"> Mista inferior (madeira e concreto)
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="valorPatrimonio" class="col-sm-6 control-label">Valor do Patrimônio - Valor do imóvel para reconstrução e reposição dos bens internos</label>
  <div class="col-sm-6">
    <div class="row">
      <div class="col-sm-4">
        <input type="text" data-toggle="tooltip" title="É fundamental que seja declarado o real valor do bem a ser segurado. Se for declarado um valor inferior ao real, poderá correr por conta do segurado parte proporcional dos prejuízos, em caso de indenização." data-mask="currency" class="form-control" id="valorPatrimonio" name="valorPatrimonio" required>
      </div>
    </div>
    <span id="helpBlock" class="help-block">Valores patrimoniais superiores a R$ 200.000,00 serão sujeitos a inspeção de risco. 
O valor a ser considerado é aquele necessário para reconstrução do imóvel e reposição dos seus bens internos no caso de um acidente que cause a destruição completa do seu negócio. Não leve em conta o valor do imóvel no mercado imobiliário, isto é, considere o valor para reconstrução e não o valor de compra/venda do imóvel.</span>
  </div>
</div>

<div class="form-group">
  <label for="sexo" class="col-sm-6 control-label">Que porcentagem do seu negócio está protegido contra incêndio?</label>
</div>

<div class="form-group">
  <label for="comExtintores" class="col-sm-6 control-label">Com extintores</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="comExtintores" id="comExtintores" value="Nada" required> Nada
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comExtintores" id="comExtintores" value="25%"> 25%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comExtintores" id="comExtintores" value="50%"> 50%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comExtintores" id="comExtintores" value="75%"> 75%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comExtintores" id="comExtintores" value="100%"> 100%
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="comHidrantes" class="col-sm-6 control-label">Com hidrantes</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="comHidrantes" id="comHidrantes" value="Nada" required> Nada
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comHidrantes" id="comHidrantes" value="25%"> 25%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comHidrantes" id="comHidrantes" value="50%"> 50%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comHidrantes" id="comHidrantes" value="75%"> 75%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comHidrantes" id="comHidrantes" value="100%"> 100%
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="comSprinklers" class="col-sm-6 control-label">Com sprinklers</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="comSprinklers" id="comSprinklers" value="Nada" required> Nada
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comSprinklers" id="comSprinklers" value="25%"> 25%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comSprinklers" id="comSprinklers" value="50%"> 50%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comSprinklers" id="comSprinklers" value="75%"> 75%
      </label>
      <label class="btn btn-default">
        <input type="radio" name="comSprinklers" id="comSprinklers" value="100%"> 100%
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="possuiSistemaDeSegurança" class="col-sm-6 control-label">Possui algum sistema de proteção/segurança?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="possuiSistemaDeSegurança" id="possuiSistemaDeSegurança" value="Nenhum" required> Nenhum
      </label>
      <label class="btn btn-default">
        <input type="radio" name="possuiSistemaDeSegurança" id="possuiSistemaDeSegurança" value="Alarme monitorado"> Alarme monitorado
      </label>
      <label class="btn btn-default">
        <input type="radio" name="possuiSistemaDeSegurança" id="possuiSistemaDeSegurança" value="Vigilância particular armada 24hs"> Vigilância particular armada 24hs
      </label>
      <label class="btn btn-default">
        <input type="radio" name="possuiSistemaDeSegurança" id="possuiSistemaDeSegurança" value="Alarme monitorado e Vigilância particular armada 24hs"> Alarme monitorado e Vigilância particular armada 24hs
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="cepEmpresa" class="col-sm-6 control-label">CEP da empresa</label>
  <div class="col-sm-6">
    <div class="row">
      <div class="col-sm-4">
        <input type="text" data-mask="cep" class="form-control" name="cepEmpresa" id="cepEmpresa" placeholder="CEP" required>
      </div>
      <h5 class="col-sm-8">
        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei meu CEP</a>
      </h5>
      <h5 class="col-sm-12 cep-result"></h5>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="numeroEmpresa" class="col-sm-6 control-label">Número da empresa</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="numeroEmpresa" name="numeroEmpresa" required>
  </div>
</div>

<div class="form-group">
  <label for="imovelTombado" class="col-sm-6 control-label">É um imóvel tombado pelo patrimônio público</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="imovelTombado" id="imovelTombado" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="imovelTombado" id="imovelTombado" value="Não"> Não
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="imovelLocalizadoNoShopping" class="col-sm-6 control-label">O imóvel está localizado em um shopping?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="imovelLocalizadoNoShopping" id="imovelLocalizadoNoShopping" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="imovelLocalizadoNoShopping" id="imovelLocalizadoNoShopping" value="Não"> Não
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="imovelAndarSuperior" class="col-sm-6 control-label">Imóvel localizado em andar superior (acima do 2º andar)?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="imovelAndarSuperior" id="imovelAndarSuperior" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="imovelAndarSuperior" id="imovelAndarSuperior" value="Não"> Não
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="imovelLocalizadoCondominioComercialFechado" class="col-sm-6 control-label">Imóvel localizado em condomínio comercial fechado?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="imovelLocalizadoCondominioComercialFechado" id="imovelLocalizadoCondominioComercialFechado" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="imovelLocalizadoCondominioComercialFechado" id="imovelLocalizadoCondominioComercialFechado" value="Não"> Não
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="atividadeExclusivamenteDeposito" class="col-sm-6 control-label">A atividade do local é exclusivamente depósito?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="atividadeExclusivamenteDeposito" id="atividadeExclusivamenteDeposito" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="atividadeExclusivamenteDeposito" id="atividadeExclusivamenteDeposito" value="Não"> Não
      </label>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="éumaRenovação" class="col-sm-6 control-label">Este seguro é uma renovação?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="éumaRenovação" id="éumaRenovação" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="éumaRenovação" id="éumaRenovação" value="Não"> Não
      </label>
    </div>
  </div>
</div>


<div class="form-group">
  <label for="nomeDoSegurado" class="col-sm-6 control-label">Nome do Cliente</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="nomeDoSegurado" name="nomeDoSegurado" required>
  </div>
</div>

<div class="form-group">
  <label for="telefoneResidencial" class="col-sm-6 control-label">Telefone Residencial</label>
  <div class="col-sm-2">
    <input type="text" data-mask="phone" class="form-control" id="telefoneResidencial" name="telefoneResidencial" required>
  </div>
</div>

<div class="form-group">
  <label for="telefoneCelular" class="col-sm-6 control-label">Telefone Celular</label>
  <div class="col-sm-2">
    <input type="text" data-mask="phone" class="form-control" id="telefoneCelular" name="telefoneCelular" required>
  </div>
</div>

<div class="form-group">
  <label for="email" class="col-sm-6 control-label">E-mail</label>
  <div class="col-sm-4">
    <input type="email" class="form-control" id="email" name="email">
  </div>
</div>