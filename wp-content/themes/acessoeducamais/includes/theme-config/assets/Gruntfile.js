module.exports = function (grunt) {
    // 1. load all grunt tasks matching the `grunt-*` pattern
  require('load-grunt-tasks')(grunt)
    // 2. All configuration goes here
  grunt.initConfig({
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'dist/css/style.min.css': 'dev/css/style.scss'
        }
      },
      dev: {
        options: {
          style: 'expanded',
          lineNumbers: true
        },
        files: {
          'dist/css/style.css': 'dev/css/style.scss'
        }
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({
            browsers: ['last 2 versions']
          })
        ]
      },
      dist: {
        src: 'dist/css/style.css'
      }
    },
    concat: {
      dist: {
        src: ['dev/bower_components/jquery/dist/jquery.min.js',
          'node_modules/jquery.inputmask/dist/jquery.inputmask.bundle.js',
          'dev/vendor/formvalidation/js/formValidation.min.js',
          'dev/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
          'dev/vendor/jquery.bootstrap.wizard.min.js',
          'dev/vendor/formvalidation/js/framework/bootstrap.min.js',
          'dev/vendor/formvalidation/js/language/pt_BR.js',
          'node_modules/moment/min/moment.min.js',
          'node_modules/masonry-layout/dist/masonry.pkgd.min.js',
          'node_modules/form-serializer/dist/jquery.serialize-object.min.js',
          'node_modules/select2/dist/js/select2.full.min.js',
          'node_modules/slick-carousel/slick/slick.min.js',
          'dev/js/pre-enrollment.js',
          'dev/js/polos.js',
          'dev/js/fotos.js',
          'dev/js/main.js'],
        dest: 'dist/js/main.js'
      }
    },
    uglify: {
      dev: {
        files: {
          'dist/js/main.min.js': ['dist/js/main.js']
        }
      }
    },
    imagemin: {
      dynamic: {                         // Another target
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: 'dev/img/',                   // Src matches are relative to this path
          src: ['*.{png,jpg,gif}'],   // Actual patterns to match
          dest: 'dist/img/'                  // Destination path prefix
        }]
      }
    },
    watch: {
      options: {
        livereload: true
      },
      css: {
        files: 'dev/css/**/*.scss',
        tasks: ['sass:dev', 'postcss']
      },
      img: {
        files: 'dev/img/*.{png,jpg,gif}',
        tasks: ['imagemin']
      },
      js: {
        files: 'dev/js/**/*.js',
        tasks: ['concat']
      },
      html: {
        files: ['*.html', 'dev/**/*.html']
      }
    },
    connect: {
      server: {
        options: {
          port: 8000,
          base: './'
        }
      }
    }
  })

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
  grunt.registerTask('default', ['sass', 'concat', 'uglify', 'imagemin', 'responsive_images'])

  grunt.registerTask('prod', ['sass:dev', 'concat', 'uglify', 'imagemin'])

  grunt.registerTask('dev', ['connect', 'watch'])
}
