<h2 class="text-center"><strong>E para terminar, quem é o responsável pelo seguro?</strong></h2>
<br>
<div class="form-group">
  <label for="nomeCompletoSegurado" class="col-sm-6 control-label">Nome completo:</label>
  <div class="col-sm-4">
		<input type="text" class="form-control" name="nomeCompletoSegurado" id="nomeCompletoSegurado" placeholder="" required>
  </div>
</div>

<div class="form-group">
  <label for="sexoContato" class="col-sm-6 control-label">Sexo</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="sexoContato" id="sexoContato" value="Masculino" required> Masculino</label>
			<label class="btn btn-default">
			  <input type="radio" name="sexoContato" id="sexoContato" value="Feminino"> Feminino
			</label>
		</div>
  </div>
</div>