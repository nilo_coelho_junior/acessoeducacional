<?php get_header(); ?>

<div class="container-fluid">
  <div class="container">
    <?php echo do_shortcode('[rev_slider alias="home"]'); ?>
  </div>

  <?php get_template_part( './includes/courses-list'); ?>
  <?php get_template_part( './includes/contact'); ?>

</div>

<?php get_footer(); ?>
