<?php

function site_style() {
  wp_enqueue_style( 'style', get_template_directory_uri() . '/includes/theme-config/assets/dist/css/style.css');
}

add_action( 'wp_enqueue_styles', 'site_style' );

function site_scripts() {
  wp_enqueue_style( 'style', get_template_directory_uri() . '/includes/theme-config/assets/dist/css/style.css');
  wp_enqueue_script( 'main', get_template_directory_uri() . '/includes/theme-config/assets/dist/js/main.js');
}

add_action( 'wp_enqueue_scripts', 'site_scripts' );

?>
