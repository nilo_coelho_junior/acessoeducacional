<?php get_header(); ?>

<div class="container-fluid">
  <div class="container">
    <h2 class="page-title"><?php the_title(); ?></h2>
  </div>

  <div class="container">
    <?php
      if ( have_posts() ) : while ( have_posts() ) : the_post();
        echo the_content();
      endwhile; endif;
    ?>
  </div>
  <br>
  <?php get_template_part( './includes/courses-list'); ?>
</div>

<?php get_footer(); ?>
