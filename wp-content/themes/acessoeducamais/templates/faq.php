<?php
global $post;
$pagename = get_query_var('pagename');

if (is_front_page() || is_page("recebemos-sua-cotacao")) {
  $faqs = new WP_Query(array(
    'post_type' => 'faq', 
    'meta_key' => 'wpcf-visivel-na-home-page', 
    'meta_value_num' => 1
  ));
  $faqs = $faqs->get_posts();
}else{
  $faqs = types_child_posts('faq');
}

$index = 0;

?>
<div class="container-fluid wrapper default faq" id="faq">
  <div class="container">
    <h1 class="text-center title">Perguntas mais frequentes sobre seguros</h1>
    <div class="row content-box">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        
        <?php 
          foreach ($faqs as $faq) {
        ?>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading<?php echo $index; ?>">
            <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $index; ?>" aria-expanded="true" aria-controls="collapse<?php echo $index; ?>">
                <span class="icon-chevron"></span>
                <?php echo $faq->post_title; ?>
              </a>
            </h4>
          </div>
          <div id="collapse<?php echo $index; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $index; ?>">
            <div class="panel-body">
              <?php echo $faq->post_content; ?>
            </div>
          </div>
        </div>
        <?php
            $index++;
          }
         ?>
      </div>
    </div>
  </div>
</div>