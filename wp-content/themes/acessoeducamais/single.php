<?php get_header(); ?>
<br>
<div class="container blog post">
	<div class="col-lg-8">
		<?php if ( have_posts() ) : ?>
	    <?php while ( have_posts() ) : the_post(); ?>
        <h1 class="post-title"><strong><?php the_title(); ?></strong></h1>
        <hr>
        <div class="meta">
          <span><?php the_time('j \d\e F \d\e Y'); ?></span>
          <span class="divider">|</span>
          <span class="categories-links">
            <?php printf('%2$s', 'entry-utility-prep entry-utility-prep-cat-links', get_the_category_list( ', ' ) ); ?>
          </span>
        </div>
        <br>
        <div class="entryhead">
	    	  <?php echo the_post_thumbnail(array('900', '450')); ?>
        </div>
	      <?php echo the_content(); ?>
        <?php if ( function_exists( "get_yuzo_related_posts" ) ) { get_yuzo_related_posts(); } ?>
	    <?php endwhile;
        echo do_shortcode('[fbcomments]');
      ?>
	  <?php endif; ?>
	  <hr>
  </div>
  <?php
  	wp_reset_query();
  	get_sidebar();
    // theme_postsbycategory();
  ?>
</div>

<?php get_footer(); ?>
