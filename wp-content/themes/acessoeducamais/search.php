<?php
/**
 * The template for displaying search results pages
 *
 */

get_header(); ?>

<div class="container-fluid">
  <div class="container">

		<header class="page-header">
			<?php if ( have_posts() ) : ?>
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyseventeen' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			<?php else : ?>
				<h1 class="page-title"><?php _e( 'Nothing Found', 'twentyseventeen' ); ?></h1>
			<?php endif; ?>
		</header><!-- .page-header -->

		<div class="col-lg-8">
			<main class="widget_siteorigin-panels-postloop" role="main">

				<?php
				if ( have_posts() ) :

					get_template_part( 'loop');

				else : ?>

					<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
					<?php
						get_search_form();

				endif;
				?>

			</main><!-- #main -->
		</div><!-- #primary -->
		<?php get_sidebar(); ?>
	</div>
</div><!-- .wrap -->

<?php get_footer();
