<h2 class="text-center"><strong>E para terminar, deixe seus dados para entrarmos em contato</strong></h2>
<br>

<div class="form-group">
  <label for="nomeDoSegurado" class="col-sm-6 control-label">Nome do Segurado</label>
  <div class="col-sm-6">
    <input type="text" class="form-control" id="nomeDoSegurado" name="nomeDoSegurado" required>
  </div>
</div>

<div class="form-group">
  <label for="telefoneResidencial" class="col-sm-6 control-label">Telefone Residencial</label>
  <div class="col-sm-2">
    <input type="text" data-mask="phone" class="form-control" id="telefoneResidencial" name="telefoneResidencial" required>
  </div>
</div>

<div class="form-group">
  <label for="telefoneCelular" class="col-sm-6 control-label">Telefone Celular</label>
  <div class="col-sm-2">
    <input type="text" data-mask="phone" class="form-control" id="telefoneCelular" name="telefoneCelular" required>
  </div>
</div>

<div class="form-group">
  <label for="email" class="col-sm-6 control-label">E-mail</label>
  <div class="col-sm-6">
    <input type="email" class="form-control" id="email" name="email" required>
  </div>
</div>