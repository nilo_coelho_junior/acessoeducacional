<?php get_header(); ?>
<br>
<div class="container blog post">
	<div class="col-lg-12">
		<?php if ( have_posts() ) : ?>
	    <?php while ( have_posts() ) : the_post(); ?>
        <h1 class="post-title"><strong><?php the_title(); ?></strong></h1>
        <hr>
        <div class="meta">
          <span><?php the_time('j \d\e F \d\e Y'); ?></span>
          <span class="divider">|</span>
          <span class="categories-links">
            <?php printf('%2$s', 'entry-utility-prep entry-utility-prep-cat-links', get_the_category_list( ', ' ) ); ?>
          </span>
        </div>
        <br>
        <div class="col-sm-10 col-sm-push-1">
          <div id="gallery-main">
            <?php
              echo(types_render_field( 'foto-galeria', array('width' => 900, 'height' => 400, 'output' => 'html')));
            ?>
          </div>
          <br>
          <div id="gallery-thumb">
            <?php echo(types_render_field( 'foto-galeria', array('size' => 'thumbnail', 'output' => 'html'))); ?>
          </div>
        </div>
	    <?php endwhile;
      ?>
	  <?php endif; ?>
	  <hr>
  </div>
</div>

<?php get_footer(); ?>
